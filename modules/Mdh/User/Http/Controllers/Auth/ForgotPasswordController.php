<?php

namespace Mdh\User\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Mdh\User\Http\Requests\PasswordVerifyCodeResetRequest;
use Mdh\User\Http\Requests\SendResetPasswordVerifyCodeRequest;
use Mdh\User\Http\Requests\VerifyCodeRequest;
use Mdh\User\Models\User;
use Mdh\User\Repositories\UserRepo;
use Mdh\User\Services\VerifyCodeService;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controllers
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    public function showVerifyCodeRequestForm()
    {
        return view('User::Front.passwords.email');
    }

    public function sendVerifyCodeEmail(SendResetPasswordVerifyCodeRequest $request)
    {
        $user = resolve(UserRepo::class)->findByEmail($request->email);

        if($user && !VerifyCodeService::checkCache($user->id)){
            $user->sendPasswordVerifyCodeNotification();
        }

        return view('User::Front.passwords.enter-verify-code-form');
    }

    public function checkVerifyCode(PasswordVerifyCodeResetRequest $request, UserRepo $user_repo)
    {
        $user = $user_repo->findByEmail($request->email);

        if(! VerifyCodeService::check($request->verify_code, $user->id)){
            return back()->withErrors(['verify_code'=> 'کد وارد شده معتبر نمی باشد']);
        }

        auth()->loginUsingId($user->id);

        return redirect()->route('password.showResetForm');
    }
}
