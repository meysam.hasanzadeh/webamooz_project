<?php

namespace Mdh\Payment\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Mdh\Payment\Gateways\Gateway;
use Mdh\Payment\Gateways\ZarinPal\ZarinPalAdaptor;
use Mdh\RolePermissions\Models\Permission;

class PaymentServiceProvider extends ServiceProvider
{
    protected string $namespace = 'Mdh\Payment\Http\Controllers';

    public function register()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(__DIR__ . '/../Routes/payment_routes.php');

        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'Payments');
        $this->loadJsonTranslationsFrom(__DIR__.'/../Resources/Lang');
    }

    public function boot()
    {
        $this->app->singleton(Gateway::class, function($app){
            return new ZarinPalAdaptor();
        });

        config()->set('sidebar.items.payments',[
            'icon'=> 'i-transactions',
            'title'=> 'تراکنش ها',
            'url'=> route('payments.index'),
            'permission' => [
                Permission::PERMISSION_MANAGE_PAYMENTS,
            ]
        ]);

        config()->set('sidebar.items.myPurchases',[
            'icon'=> 'i-my__purchases',
            'title'=> 'خرید های من',
            'url'=> route('payments.my_purchases'),
        ]);
    }
}
