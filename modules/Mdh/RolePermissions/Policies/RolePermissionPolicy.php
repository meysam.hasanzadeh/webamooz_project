<?php

namespace Mdh\RolePermissions\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Mdh\RolePermissions\Models\Permission;
use Mdh\User\Models\User;
use phpDocumentor\Reflection\Types\True_;
use function Composer\Autoload\includeFile;

class RolePermissionPolicy
{
    use HandlesAuthorization;


    public function index($user)
    {
        if($user->hasPermissionTo(Permission::PERMISSION_MANAGE_ROLE_PERMISSION)) return true;

        return null;
    }

    public function create($user)
    {
        if($user->hasPermissionTo(Permission::PERMISSION_MANAGE_ROLE_PERMISSION)) return true;

        return null;
    }

    public function edit($user)
    {
        if($user->hasPermissionTo(Permission::PERMISSION_MANAGE_ROLE_PERMISSION)) return true;

        return null;
    }

    public function delete($user)
    {
        if($user->hasPermissionTo(Permission::PERMISSION_MANAGE_ROLE_PERMISSION)) return true;

        return null;

    }
}
