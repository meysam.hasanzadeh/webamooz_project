@component('mail::message')
# کد تایید ایمیل

این ایمیل مربوط به ثبت نام شما در سایت است

@component('mail::panel')
کد شما :{{ $code }}
@endcomponent

با تشکر,<br>
{{ config('app.name') }}
@endcomponent
