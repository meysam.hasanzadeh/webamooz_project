<?php


use Illuminate\Support\Facades\Route;

Route::group(['namespace'=>'Mdh\Dashboard\Http\Controllers', 'middleware'=> ['web','auth','verified']], function ($router){
   $router->get('/home', 'HomeController@index')->name('home');
});
