<?php


namespace Mdh\Course\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Mdh\Course\Listeners\RegisterUserInTheCourse;
use Mdh\Payment\Events\PaymentWasSuccessful;


class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        PaymentWasSuccessful::class => [
            RegisterUserInTheCourse::class
        ]
    ];

    public function boot()
    {
        //
    }
}
