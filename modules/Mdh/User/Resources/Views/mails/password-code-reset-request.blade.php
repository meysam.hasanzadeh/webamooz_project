@component('mail::message')
# کد بازیابی رمز عبور

این ایمیل مربوط به بازیابی رمز عبور شما در سایت است

@component('mail::panel')
کد شما :{{ $code }}
@endcomponent

با تشکر,<br>
{{ config('app.name') }}
@endcomponent
