<p class="box__title">ایجاد نقش کاربری جدید</p>
<form action="{{ route('role-permissions.store') }}" method="post" class="padding-30">
    @csrf
    <input type="text" name="name" placeholder="عنوان" class="text @error('name') is-invalid @enderror"
           value="{{ old("name") }}"
    >
    @error('name')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror

    <p class="box__title margin-bottom-15">انتخاب مجوزها</p>
    @foreach($permissions as $permission)
        <label class="ui-checkbox padding-8-20">
            <input type="checkbox" name="permissions[{{$permission->name}}]" class="sub-checkbox"
                    data-id="1" value="{{ $permission->name }}"  @if(is_array(old('permissions')) &&
                    array_key_exists($permission->name, old('permissions'))) checked @endif
            >
            <span class="checkmark"></span>
            <p class="margin-15">@lang($permission->name)</p>
        </label>
    @endforeach
    @error('permissions')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
    <br>
    <button class="btn btn-webamooz_net">اضافه کردن</button>
</form>
