@extends('Dashboard::master')
@section('breadcrumb')
    <li><a title="ویرایش کاربر">ویرایش پروفایل</a></li>
@endsection
@section('content')
    <div class="row no-gutters margin-bottom-20">
        <div class="col-12 bg-white">
            <p class="box__title">بروزرسانی پروفایل</p>
            <x-user-photo />
            <form action="{{ route('users.updateProfile')}}" class="padding-30" method="post">
                @csrf
                @method('PATCH')
                <x-input type="text" classDiv="mb-4" value="{{ auth()->user()->name }}" class="text" name="name"
                         placeholder="نام کاربر" required
                />
                <x-input type="email" classDiv="mb-4" value="{{ auth()->user()->email }}" class="text text-left"
                         name="email" placeholder="ایمیل" required
                />
                <x-input type="mobile" classDiv="mb-4" value="{{ auth()->user()->mobile }}" class="text text-left"
                         name="mobile" placeholder="موبایل" required
                />
                <div class="d-flex multi-text mb-3">
                    <x-input type="password" classDiv="width-100" value=""
                             class="text" name="password" placeholder="رمز عبور جدید"
                    />
                </div>
                @can(\Mdh\RolePermissions\Models\Permission::PERMISSION_TEACH)
                <p class="rules font-size-13 mb-4">رمز عبور باید حداقل ۶ کاراکتر و ترکیبی از حروف بزرگ، حروف کوچک، اعداد و کاراکترهای
                    غیر الفبا مانند <strong>!@#$%^&amp;*()</strong> باشد.</p>
                <x-input type="number" classDiv="mb-4" value="{{ auth()->user()->card_number }}" class="text text-left"
                         name="card_number" placeholder="شماره کارت"
                />
                <x-input type="number" classDiv="mb-4" value="{{ auth()->user()->shaba }}" class="text text-left"
                         name="shaba" placeholder="شماره شبا"
                />
                <x-input type="text" classDiv="mb-4" value="{{ auth()->user()->username }}" class="text" name="username"
                         placeholder="نام کاربری و آدرس پروفایل"
                />
{{--                <p class="input-help text-left margin-bottom-12 font-size-13" dir="ltr">--}}
{{--                    https://webamooz.net/tutors/--}}
{{--                    <a href="https//webamooz/tutors/Mohammadnikoo">{{ auth()->user()->username }}</a>--}}
{{--                </p>--}}
                @endcan
                <div>
                    <button class="btn btn-webamooz_net">بروزرسانی</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('js')
    <script src="/panel/js/tagsInput.js?v={{ uniqid() }}"></script>

    <script>
        @include('Common::feedback')
    </script>
@endsection
