<?php


namespace Mdh\Course\Repositories;

use Illuminate\Support\Str;
use Mdh\Course\Models\Course;
use Mdh\Course\Models\Season;


class SeasonRepo
{
    public function all()
    {
        return Season::all();
    }

    public function finByIdAndCourseId($seasonId, $courseId)
    {
        return Season::where('course_id', $courseId)->where('id', $seasonId);
    }

    public function store($id, $values)
    {
        return Season::query()->create([
            'title'=> $values->title,
            'number'=> $this->generateNumber($id, $values->number),
            'course_id'=> $id,
            'user_id'=> auth()->id(),
            'confirmation_status'=> Season::CONFIRMATION_STATUS_PENDING,
            'status'=> Season::STATUS_OPENED
        ]);
    }

    public function getSeasonCourse($course_id)
    {
        return Season::where('course_id', $course_id)
            ->where('confirmation_status',Season::CONFIRMATION_STATUS_ACCEPTED)
            ->orderBy('number')
            ->get();
    }

    public function paginate()
    {
        return Season::paginate();
    }

    public function findById($id)
    {
        return Season::findOrFail($id);
    }

    public function allExceptByID($id)
    {
        return $this->all()->filter(function ($item) use($id) {
             return $item->id != $id;
        });
    }

    public function update($id, $values)
    {
        $season_repo = new SeasonRepo();
        $season = $season_repo->findById($id);
        $course_id = $season->course_id;

        return Season::where('id',$id)->update([
            'title'=> $values->title,
            'number'=>  $this->generateNumber($course_id, $values->number),
        ]);
    }

    public function delete($id)
    {
        return Season::where('id',$id)->delete();
    }

    public function updateConfirmationStatus($id, $status)
    {
        return Season::where('id',$id)->update([
           'confirmation_status'=> $status
        ]);
    }

    public function updateStatus($id, $status)
    {
        return Season::where('id',$id)->update([
            'status'=> $status
        ]);
    }

    /**
     * @param $courseId
     * @param $number
     * @return int
     */
    public function generateNumber($courseId, $number): int
    {
        $course_repo = new CourseRepo();

        if (is_null($number)) {
            $number = $course_repo->findById($courseId)->seasons()->orderBy('number', 'desc')->firstOrNew([])->number ?: 0;
            $number++;
        }
        return $number;
    }
}
