<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace'=>'Mdh\RolePermissions\Http\Controllers', 'middleware'=> ['web','auth','verified']], function ($router){
    $router->resource('role-permissions','RolePermissionsController');
});
