<?php


namespace Mdh\Media\Contracts;


use Illuminate\Http\UploadedFile;
use Mdh\Media\Models\Media;

interface FileServiceContract
{
    public static function upload(UploadedFile $file, string $fileName, string $dir): array;

    public static function delete(Media $media);

    public static function thumb(Media $media);

    public static function stream(Media $media);
}
