<?php

namespace Mdh\Payment\Services;

use Mdh\Payment\Gateways\Gateway;
use Mdh\Payment\Models\Payment;
use Mdh\Payment\Repositories\PaymentRepo;
use Mdh\User\Models\User;

class PaymentService
{
    public static function generate($amount, $paymentable, User $buyer)
    {
        if ($amount <= 0 || is_null($paymentable) || is_null($buyer)) return false;

        $gateway = resolve(Gateway::class);

        $invoiceId = $gateway->request($amount,$paymentable->title);

        if (is_array($invoiceId))
        {
            dd("hello");
        }

        if (!is_null($paymentable->percent))
        {
            $seller_p = $paymentable->percent;
            $seller_share = ($amount / 100) * $seller_p;
            $site_share = $amount - $seller_share;
        }else {
            $seller_p = $seller_share = 0;
            $site_share = $amount;
        }


        return resolve(PaymentRepo::class)->store([
            'buyer_id'=> $buyer->id,
            'paymentable_id'=> $paymentable->id,
            'paymentable_type'=> get_class($paymentable),
            'amount'=> $amount,
            'invoice_id'=> $invoiceId,
            'gateway'=> $gateway->getName(),
            'status'=> Payment::STATUS_PENDING,
            'seller_p'=> $seller_p,
            'seller_share'=> $seller_share,
            'site_share'=> $site_share,
        ]);
    }
}


//class PaymentService
//{
//    public static function generate($amount, $paymentable, User $buyer)
//    {
//        if ($amount <= 0 || is_null($paymentable) || is_null($buyer)) return false;
//
//        $data_result = self::result($amount, $paymentable, $buyer);
//
//        return resolve(PaymentRepo::class)->store($data = self::createPayment($data_result));
//    }
//
//    public static function createPayment($data_result)
//    {
//        return $data_result;
//    }
//
//    public static function checkPayment($paymentable,$amount)
//    {
//        if (!is_null($paymentable->percent))
//        {
//            $seller_p = $paymentable->percent;
//            $seller_share = ($amount / 100) * $seller_p;
//            $site_share = $amount - $seller_share;
//        }else {
//            $seller_p = $seller_share = $site_share = 0;
//        }
//
//        return [$seller_p,$seller_share,$site_share];
//    }
//
//    public static function result($amount, $paymentable, $buyer)
//    {
//        [$seller_p,$seller_share, $site_share] = self::checkPayment($paymentable,$amount);
//
//        return [
//            'buyer_id'=> $buyer->id,
//            'paymentable_id'=> $paymentable->id,
//            'paymentable_type'=> get_class($paymentable),
//            'amount'=> $amount,
//            'invoice_id'=> 0,
//            'gateway'=> "",
//            'status'=> Payment::STATUS_PENDING,
//            'seller_p'=> $seller_p,
//            'seller_share'=> $seller_share,
//            'site_share'=> $site_share,
//        ];
//    }
//
//}
