<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace'=>'Mdh\Course\Http\Controllers', 'middleware'=> ['web','auth','verified']], function ($router){
    $router->patch('lessons/{lesson}/accept', 'LessonController@accept')->name('lessons.accept');
    $router->patch('courses/{course}/accept-all', 'LessonController@acceptAll')->name('lessons.acceptAll');
    $router->patch('courses/{course}/accept-multiple', 'LessonController@acceptMultiple')->name('lessons.acceptSelected');
    $router->patch('courses/{course}/reject-multiple', 'LessonController@rejectMultiple')->name('lessons.rejectMultiple');
    $router->patch('lessons/{lesson}/reject', 'LessonController@reject')->name('lessons.reject');
    $router->patch('lessons/{lesson}/lock', 'LessonController@lock')->name('lessons.lock');
    $router->patch('lessons/{lesson}/unlock', 'LessonController@unlock')->name('lessons.unlock');
    $router->get('course/{course}/lessons/create', 'LessonController@create')->name('lessons.create');
    $router->post('course/{course}/lessons/store', 'LessonController@store')->name('lessons.store');
    $router->get('course/{course}//lessons/{lesson}/edit', 'LessonController@edit')->name('lessons.edit');
    $router->patch('course/{course}//lessons/{lesson}/update', 'LessonController@update')->name('lessons.update');
    $router->delete('course/{course}/lessons/{lesson}', 'LessonController@destroy')->name('lessons.destroy');
    $router->delete('course/{course}/lessons', 'LessonController@destroyMultiple')->name('lessons.destroyMultiple');
});
