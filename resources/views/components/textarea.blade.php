
<div class="mb-4">
    <textarea placeholder="{{ $placeholder }}" name="{{ $name }}" class="text h margin-bottom-0">
        {!! isset($value) ? $value : old($name) !!}
    </textarea>
    <x-validation-error field="{{ $name }}"/>
</div>
