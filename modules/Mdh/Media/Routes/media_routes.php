<?php


use Illuminate\Support\Facades\Route;

Route::group([], function ($router)
{
    $router->get('/media/{media}/download', 'MediaController@download')->name('media.download');
});
