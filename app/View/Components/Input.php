<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Input extends Component
{

    public $type;
    public $classDiv;
    public $class;
    public $name;
    public $placeholder;


    public function __construct($type, $class, $name, $placeholder,$classDiv="")
    {
        $this->type = $type;
        $this->classDiv = $classDiv;
        $this->class = $class;
        $this->name = $name;
        $this->placeholder = $placeholder;
    }

    public function render()
    {
        return view('components.input');
    }
}
