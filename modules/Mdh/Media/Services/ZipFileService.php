<?php


namespace Mdh\Media\Services;


use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Mdh\Media\Contracts\FileServiceContract;
use Mdh\Media\Models\Media;

class ZipFileService extends DefaultFileService implements FileServiceContract
{

    public static function upload(UploadedFile $file, string $fileName, string $dir): array
    {
        Storage::putFileAs($dir, $file, $fileName .'.'. $file->getClientOriginalExtension());
        return ["zip"=> $fileName.'.'.$file->getClientOriginalExtension()];
    }

    public static function thumb(Media $media)
    {
        return url("img/zip-thumb.png");
    }

    static function getFileName()
    {
        return (static::$media->is_private ? 'private/' : 'public/').static::$media->files['zip'];
    }
}
