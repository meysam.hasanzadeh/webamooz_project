
<div class="{{ $classDiv }}">
    <input type="{{ $type }}" class="{{ $class }}" name="{{ $name }}" placeholder="{{ $placeholder }}"
        {{ $attributes }}
        value="{{ old($name) }}"
    >
    <x-validation-error field="{{ $name }}"/>
</div>
