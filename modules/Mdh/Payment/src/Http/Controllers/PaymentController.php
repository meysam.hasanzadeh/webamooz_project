<?php

namespace Mdh\Payment\Http\Controllers;


use App\Http\Controllers\Controller;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Mdh\Payment\Events\PaymentWasSuccessful;
use Mdh\Payment\Gateways\Gateway;
use Mdh\Payment\Models\Payment;
use Mdh\Payment\Repositories\PaymentRepo;
use Morilog\Jalali\Jalalian;


class PaymentController extends Controller
{
    public function index(PaymentRepo $paymentRepo, Request $request)
    {
        $this->authorize('manage', Payment::class);
        $last30DaysTotal = $paymentRepo->getLastNDaysTotal(-30);
        $last30DaysBenefit = $paymentRepo->getLastNDaysSiteBenefit(-30);
        $last30DaysSellerShare = $paymentRepo->getLastNDaysSellerShare(-30);
        $lastTotalSell = $paymentRepo->getLastNDaysTotal();
        $lastTotalBenefit = $paymentRepo->getLastNDaysSiteBenefit();

        $dates = collect();
        foreach (range(-30,0) as $i){
            $dates->put(now()->addDays($i)->format("Y-m-d"),0);
        }

        $summery = $paymentRepo->getDailySummery($dates);
        $last30Days = CarbonPeriod::create(now()->addDays(-30),now());
        $payments = $paymentRepo
            ->searchByEmail($request->email)
            ->searchByAmount($request->amount)
            ->searchByInvoiceId($request->invoice_id)
            ->searchAfterDate(dateFromJalali($request->start_date))
            ->searchBeforeDate(dateFromJalali($request->end_date))
            ->paginate();

        return view(
            "Payments::index", compact(
                'payments',
                'last30DaysTotal',
                'last30DaysBenefit',
                'lastTotalSell',
                'lastTotalBenefit',
                'last30Days',
                'last30DaysSellerShare',
                'summery',
                'dates',
            )
        );
    }

    public function callback(Request $request, PaymentRepo $paymentRepo)
    {
        $gateway = resolve(Gateway::class);
        $payment = $paymentRepo->findByInvoiceId($gateway->getInvoiceIdFromRequest($request));
        if (!$payment) {
            newFeedback('تراکنش ناموفق', 'تراکنش مورد نظر یافت نشد!', 'error');
            return redirect('/');
        }

        $result = $gateway->verify($payment);

        if (is_array($result))
        {
            newFeedback('عملیات ناموفق', $result['message'], 'error');
            $paymentRepo->changeStatus($payment->id, Payment::STATUS_FAIL);
        }else {
            event(new PaymentWasSuccessful($payment));
            newFeedback('عملیات موفق', 'پرداخت با موفقیت انجام شد', 'success');
            $paymentRepo->changeStatus($payment->id, Payment::STATUS_SUCCESS);
        }


        return redirect()->to($payment->paymentable->path());
    }

    public function myPurchases(Request $request)
    {
        $myPurchases = $request->user()->payments()->paginate();
        return view('Payments::my_payments',compact('myPurchases'));
    }
}
