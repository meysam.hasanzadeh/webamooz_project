<?php

namespace Mdh\Course\Rules;

use Illuminate\Contracts\Validation\Rule;
use Mdh\User\Repositories\UserRepo;

class ValidTeacher implements Rule
{

    private UserRepo $userRepo;

    public function __construct()
    {
        $this->userRepo = resolve(UserRepo::class);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = $this->userRepo->findById($value);

        return $user->hasPermissionTo('teach');
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'کاربر انتخاب شده یک مدرس معتبر نیست.!';
    }
}
