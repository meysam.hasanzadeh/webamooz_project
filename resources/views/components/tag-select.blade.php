<ul class="tags mb-4">
    <li class="tagAdd taglist">
        <input type="{{ $type }}" name="{{ $name }}" id="search-field" placeholder="{{ $placeholder }}">
    </li>
</ul>
<p class="mb-3">
    <x-validation-error field="{{ $name }}"/>
</p>
