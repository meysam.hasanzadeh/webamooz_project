<?php

return [
    'items'=>[
        'dashboard'=>[],
        'courses'=>[],
        'categories'=>[],
        'users'=> [],
        'role-permissions'=>[],
        'profile'=> [],
        'payments'=> [],
        'myPurchases'=> [],
    ]
];
