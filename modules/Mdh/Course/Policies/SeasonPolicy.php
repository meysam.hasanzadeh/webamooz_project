<?php

namespace Mdh\Course\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Mdh\RolePermissions\Models\Permission;
use Mdh\User\Models\User;

class SeasonPolicy
{
    use HandlesAuthorization;

    public function manage($user)
    {
        if($user->hasPermissionTo(Permission::PERMISSION_MANAGE_COURSES)) return true;

        return null;
    }

    public function create($user)
    {
        return $user->hasPermissionTo(Permission::PERMISSION_MANAGE_COURSES) ||
               $user->hasPermissionTo(Permission::PERMISSION_MANAGE_OWN_COURSES);
    }

    public function edit($user, $season)
    {
        if($user->hasPermissionTo(Permission::PERMISSION_MANAGE_COURSES)) return true;

        return $user->hasPermissionTo(Permission::PERMISSION_MANAGE_OWN_COURSES) && $season->course->teacher_id = $user->id;
    }

    public function delete($user, $season)
    {
        if($user->hasPermissionTo(Permission::PERMISSION_MANAGE_COURSES)) return true;

        return $user->hasPermissionTo(Permission::PERMISSION_MANAGE_OWN_COURSES) && $season->course->teacher_id = $user->id;
    }

    public function change_confirmation_status($user)
    {
        if($user->hasPermissionTo(Permission::PERMISSION_MANAGE_COURSES)) return true;

        return null;
    }
}
