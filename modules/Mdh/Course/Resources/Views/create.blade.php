@extends('Dashboard::master')
@section('breadcrumb')
    <li><a href="{{ route('courses.index') }}" title="دوره ها">دوره ها</a></li>
    <li><a title="ویرایش دوره">ایجاد دوره</a></li>
@endsection
@section('content')
    <div class="row no-gutters">
        <div class="col-12 bg-white">
            <p class="box__title">بروزرسانی دوره</p>
            <form action="{{ route('courses.store') }}" class="padding-30" method="post" enctype="multipart/form-data">
                @csrf
                <x-input type="text" classDiv="mb-4" class="text" name="title" placeholder="عنوان دوره" required/>
                <x-input type="text" classDiv="mb-4" class="text text-left" name="slug" placeholder="نام انگلیسی دوره" required/>

                <div class="d-flex multi-text mb-3">
                    <x-input type="text" classDiv="width-100 margin-left-20" class="text text-left" name="priority" placeholder="ردیف دوره" />
                    <x-input type="text" classDiv="width-100 margin-left-20 mb-3" class="text text-left" name="price" placeholder="مبلغ دوره" required/>
                    <x-input type="number" classDiv="width-100 mb-3" class="text text-left" name="percent" placeholder="درصد مدرس" required/>
                </div>

                <x-select name="teacher_id" class="margin-bottom-0" required>
                    <option value="">انتخاب مدرس دوره</option>
                    @foreach($teachers as $teacher)
                    <option value="{{ $teacher->id }}" @if($teacher->id == old('teacher_id'))
                    selected @endif>{{ $teacher->name }}</option>
                    @endforeach
                </x-select>

                <x-tag-select type="text" name="tags" placeholder="برچسب ها"/>

                <x-select name="type" class="margin-bottom-0" required>
                    <option value="">نوع دوره</option>
                    @foreach(\Mdh\Course\Models\Course::$types as $type)
                    <option value="{{ $type }}" @if($type == old('type')) selected @endif>
                        @lang($type)
                    </option>
                    @endforeach
                </x-select>

                <x-select name="status" class="margin-bottom-0">
                    <option value="">وضعیت دوره</option>
                    @foreach(\Mdh\Course\Models\Course::$statuses as $status)
                    <option value="{{ $status }}" @if($status == old('status')) selected @endif>@lang($status)</option>
                    @endforeach
                </x-select>

                <x-select name="category_id" class="margin-bottom-0" required>
                    <option value="">دسته بندی</option>
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}" @if($category->id == old('category_id')) selected @endif>
                            {{ $category->title }}
                        </option>
                    @endforeach
                </x-select>


                <x-file placeholder="آپلود بنر دوره" name="image"/>

                <x-textarea placeholder="توضیحات دوره" name="body"/>
                <div>
                    <button class="btn btn-webamooz_net">ایجاد دوره</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('js')
<script src="/panel/js/tagsInput.js?v={{ uniqid() }}"></script>
@endsection
