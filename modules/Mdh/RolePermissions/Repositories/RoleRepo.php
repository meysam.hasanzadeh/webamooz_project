<?php


namespace Mdh\RolePermissions\Repositories;




use Spatie\Permission\Models\Role;

class RoleRepo
{
    public function all()
    {
        return Role::all();
    }

    public function create($values)
    {
        return Role::create([
            'name'=> $values->name,
        ])->syncPermissions($values->permissions);
    }

    public function findById($id)
    {
        return Role::findOrFail($id);;
    }

    public function update($id, $value)
    {
        $role = $this->findById($id);
        return $role->syncPermissions($value->permissions)->update(['name'=> $value->name]);
    }

    public function delete($id)
    {
        return Role::where('id',$id)->delete();
    }
}
