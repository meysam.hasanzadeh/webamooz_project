<?php


use Morilog\Jalali\Jalalian;

function newFeedback($title="عملیات موفق", $body="عملیات موفق آمیز بود", $type="success")
{

    $session = session()->has('feedback') ? session()->get('feedback'): [];
    $session[] = ['title'=> $title, "body"=> $body, "type"=> $type];

    session()->flash('feedback',$session);
}

function dateFromJalali($date,$format="Y/m/d")
{
    return $date ? Jalalian::fromFormat($format,$date)->toCarbon() : null;
}

function getJalaliFromFormat($date,$format="Y-m-d")
{
    return Jalalian::fromCarbon(\Carbon\Carbon::createFromFormat($format,$date))->format($format);
}

function createFromCarbon(\Carbon\Carbon $carbon)
{
    return Jalalian::fromCarbon($carbon);
}
