<?php


namespace Mdh\Category\Repositories;


use Mdh\Category\Models\Category;
use Mdh\User\Models\User;

class CategoryRepo
{
    public function all()
    {
        return Category::all();
    }

    public function tree()
    {
        return Category::where('parent_id', null)->with('childCategories')->get();
    }

    public function store($values)
    {
        return Category::create([
            'title'=> $values->title,
            'slug'=> $values->slug,
            'parent_id'=> $values->parent_id
        ]);
    }

    public function findById($id)
    {
        return Category::findOrFail($id);
    }

    public function allExceptByID($id)
    {
        return $this->all()->filter(function ($item) use($id) {
             return $item->id != $id;
        });
    }

    public function update($id, $values)
    {
        return Category::where('id',$id)->update([
            'title'=> $values->title,
            'slug'=> $values->slug,
            'parent_id'=> $values->parent_id
        ]);
    }

    public function delete($id)
    {
        Category::where('id',$id)->delete();
    }
}
