@extends('Dashboard::master')
@section('breadcrumb')
    <li><a href="{{ route('courses.index') }}" title="دوره ها">دوره ها</a></li>
    <li><a href="{{ route('courses.details', $course->id) }}" title="دوره ها">جزییات دوره</a></li>
    <li><a title="بروزرسانی">بروز رسانی درس</a></li>
@endsection
@section('content')
    <div class="row no-gutters">
        <div class="col-12 bg-white">
            <p class="box__title">بروزرسانی دوره</p>
            <form action="{{ route('lessons.update', [$course->id, $lesson->id]) }}" class="padding-30" method="post"
                enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                <x-input type="text" classDiv="mb-4" class="text" value="{{ $lesson->title }}" name="title" placeholder="نام درس" required/>
                <x-input type="text" classDiv="mb-4" class="text text-left" value="{{ $lesson->slug }}" name="slug" placeholder="نام انگلیسی درس:اختیاری"/>
                <x-input type="number" classDiv="mb-4" class="text text-left" value="{{ $lesson->time }}" name="time" placeholder="مدت زمان جلسه" required/>
                <x-input type="number" classDiv="mb-4" class="text text-left" value="{{ $lesson->number }}" name="number" placeholder="ترتیب دوره"/>

                @if(count($course->seasons))
                    <x-select name="season_id" class="margin-bottom-0" required>
                        <option value="">انتخاب سرفصل درس</option>
                        @foreach($course->seasons as $seasonItem)
                            <option value="{{ $seasonItem->id }}" {{ $seasonItem->id == $lesson->season_id ? 'selected' : '' }}>{{ $seasonItem->title }}</option>
                        @endforeach
                    </x-select>
                @endif

                <div class="w-50">
                    <p class="box__title">ایا این درس رایگان است ؟ </p>
                    <div class="notificationGroup">
                        <input id="lesson-upload-field-1" name="is_free" value="0" type="radio" {{ !$lesson->is_free ? 'checked' : '' }}>
                        <label for="lesson-upload-field-1">خیر</label>
                    </div>
                    <div class="notificationGroup">
                        <input id="lesson-upload-field-2" name="is_free" value="1" type="radio" {{ $lesson->is_free ? 'checked' : '' }}>
                        <label for="lesson-upload-field-2">بله</label>
                    </div>
                </div>

                <x-file placeholder="آپلود درس" name="lesson_file" :value="$lesson->media"/>

                <x-textarea placeholder="توضیحات درسس" name="body" value="{{ $lesson->body }}"/>

                <div>
                    <button class="btn btn-webamooz_net">بروزرسانی دوره</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('js')
    <script src="/panel/js/tagsInput.js?v={{ uniqid() }}"></script>
@endsection
