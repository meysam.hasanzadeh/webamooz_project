@extends('Dashboard::master')
@section('breadcrumb')
    <li><a href="{{ route('courses.index') }}" title="دوره ها">دوره ها</a></li>
    <li><a href="{{ route('courses.details', $course->id) }}" title="{{ $course->title }}">{{ $course->title }}</a></li>
    <li><a title="ویرایش دوره">ایجاد درس</a></li>
@endsection
@section('content')
    <div class="row no-gutters">
        <div class="col-12 bg-white">
            <p class="box__title">ایجاد درس</p>
            <form action="{{ route('lessons.store', $course->id) }}" class="padding-30" method="post" enctype="multipart/form-data">
                @csrf
                <x-input type="text" classDiv="mb-4" class="text" name="title" placeholder="نام درس" required/>
                <x-input type="text" classDiv="mb-4" class="text text-left" name="slug" placeholder="نام انگلیسی درس:اختیاری"/>
                <x-input type="number" classDiv="mb-4" class="text text-left" name="time" placeholder="مدت زمان جلسه" required/>
                <x-input type="number" classDiv="mb-4" class="text text-left" name="number" placeholder="ترتیب دوره"/>

                @if(count($seasons))
                    <x-select name="season_id" class="margin-bottom-0" required>
                        <option value="">انتخاب سرفصل درس</option>
                        @foreach($seasons as $season)
                        <option value="{{ $season->id }}" {{old('season_id')}}>{{ $season->title }}</option>
                        @endforeach
                    </x-select>
                @endif

                <div class="w-50">
                    <p class="box__title">ایا این درس رایگان است ؟ </p>
                    <div class="notificationGroup">
                        <input id="lesson-upload-field-1" name="is_free" value="0" type="radio" checked="">
                        <label for="lesson-upload-field-1">خیر</label>
                    </div>
                    <div class="notificationGroup">
                        <input id="lesson-upload-field-2" name="is_free" value="1" type="radio">
                        <label for="lesson-upload-field-2">بله</label>
                    </div>
                </div>

                <x-file placeholder="آپلود درس" name="lesson_file"/>

                <x-textarea placeholder="توضیحات درسس" name="body"/>
                <div>
                    <button class="btn btn-webamooz_net">ایجاد درس</button>
                </div>
            </form>
        </div>
    </div>
@endsection
