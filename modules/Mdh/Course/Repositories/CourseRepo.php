<?php


namespace Mdh\Course\Repositories;

use Illuminate\Support\Str;
use Mdh\Course\Models\Course;
use Mdh\Course\Models\Lesson;
use phpDocumentor\Reflection\Types\This;


class CourseRepo
{
    public function all()
    {
        return Course::all();
    }

    public function store($values)
    {
        return Course::query()->create([
            'title'=> $values->title,
            'slug'=> Str::slug($values->slug),
            'priority'=> $values->priority,
            'price'=> $values->price,
            'percent'=> $values->percent,
            'teacher_id'=> $values->teacher_id,
            'type'=> $values->type,
            'status'=> $values->status,
            'category_id'=> $values->category_id,
            'banner_id'=> $values->banner_id,
            'body'=> $values->body,
            'confirmation_status'=> Course::CONFIRMATION_STATUS_PENDING,
        ]);
    }

    public function paginate()
    {
        return Course::paginate();
    }

    public function findById($id)
    {
        return Course::findOrFail($id);
    }

    public function getLessons($courseId)
    {
        return $this->getLessonsQuery($courseId)->get();
    }

    public function getLessonCount($courseId)
    {
        return $this->getLessonsQuery($courseId)->count();
    }

    public function getDuration($courseId)
    {
        return $this->getLessonsQuery($courseId)->sum('time');
    }

    public function latestCourses()
    {
        return Course::where('confirmation_status', Course::CONFIRMATION_STATUS_ACCEPTED)->latest()->take(8)->get();
    }

    public function allExceptByID($id)
    {
        return $this->all()->filter(function ($item) use($id) {
             return $item->id != $id;
        });
    }

    public function findByUserId($user_id)
    {
        return Course::where('teacher_id', $user_id)->paginate();
    }

    public function update($id, $values)
    {
        return Course::where('id',$id)->update([
            'title'=> $values->title,
            'slug'=> Str::slug($values->slug),
            'priority'=> $values->priority,
            'price'=> $values->price,
            'percent'=> $values->percent,
            'teacher_id'=> $values->teacher_id,
            'type'=> $values->type,
            'status'=> $values->status,
            'category_id'=> $values->category_id,
            'banner_id'=> $values->banner_id,
            'body'=> $values->body,
        ]);
    }

    public function delete($id)
    {
        Course::where('id',$id)->delete();
    }

    public function updateConfirmationStatus($id, $status)
    {
        return Course::where('id',$id)->update([
           'confirmation_status'=> $status
        ]);
    }

    public function updateStatus($id, $status)
    {
        return Course::where('id',$id)->update([
            'status'=> $status
        ]);
    }

    public function addStudentToCourse(Course $course, $studentId)
    {
        if (!$this->getCourseStudentById($course,$studentId))
        {
            $course->students()->attach($studentId);
        }
    }

    public function getCourseStudentById(Course $course, $studentId)
    {
        return $course->students()->where('id',$studentId)->first();
    }

    public function hasStudent(Course $course, $student_id)
    {
        return $course->students->contains($student_id);
    }

    private function getLessonsQuery($courseId)
    {
        return Lesson::where('course_id', $courseId)
            ->where('confirmation_status', Lesson::CONFIRMATION_STATUS_ACCEPTED);
    }
}
