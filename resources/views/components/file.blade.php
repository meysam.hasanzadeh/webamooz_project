<div class="file-upload">
    <div class="i-file-upload">
        <span>{{ $placeholder }}</span>
        <input type="file" class="file-upload" id="files" name="{{ $name }}" {{ $attributes }}/>
    </div>
    <x-validation-error field="{{ $name }}"/>
    <span class="filesize"></span>
    @if(isset($value))
        <p>{{ $value->filename }}</p>
        <img src="{{ $value->thumb }}" alt="" width="100">
    @else
        <span class="selectedFiles">فایلی انتخاب نشده است</span>
    @endif
</div>
