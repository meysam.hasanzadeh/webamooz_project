<?php


namespace Mdh\User\Http\Controllers;


use App\Http\Controllers\Controller;
use http\Env\Request;
use Mdh\Common\Responses\AjaxResponses;
use Mdh\Course\Models\Course;
use Mdh\Course\Repositories\CourseRepo;
use Mdh\Media\Services\MediaFileService;
use Mdh\RolePermissions\Models\Role;
use Mdh\RolePermissions\Repositories\RoleRepo;
use Mdh\User\Http\Requests\AddRoleRequest;
use Mdh\User\Http\Requests\UpdateProfileInformationRequest;
use Mdh\User\Http\Requests\UpdateUserPhotoRequest;
use Mdh\User\Http\Requests\UpdateUserRequest;
use Mdh\User\Models\User;
use Mdh\User\Repositories\UserRepo;

class UserController extends Controller
{
    /**
     * @var UserRepo
     */
    private UserRepo $user_repo;

    public function __construct(UserRepo $userRepo)
    {
        $this->user_repo = $userRepo;
    }

    public function index(RoleRepo $role_repo)
    {
        $this->authorize('index', User::class);
        $users = $this->user_repo->paginate();
        $roles = $role_repo->all();

        return view('User::Admin.index', compact('users','roles'));
    }

    public function edit($user_id, RoleRepo $role_repo, CourseRepo $course_repo)
    {
        $this->authorize('edit', User::class);
        $user = $this->user_repo->findById($user_id);
        $roles = $role_repo->all();

        return view('User::Admin.edit', compact('user','roles'));
    }

    public function updatePhoto(UpdateUserPhotoRequest $request)
    {
        $this->authorize('editProfile', User::class);
        $photo_id = MediaFileService::publicUpload($request->file('userPhoto'))->id;

        if (auth()->user()->image)
        {
            auth()->user()->image->delete();
        }
        auth()->user()->image_id = $photo_id;
        auth()->user()->save();
        newFeedback();

        return back();
    }

    public function viewProfile()
    {
        $this->authorize('editProfile', User::class);
        return view('User::Admin.profile');
    }

    public function updateProfile(UpdateProfileInformationRequest $request)
    {
        $this->authorize('editProfile', User::class);

        $this->user_repo->updateProfile($request);

        newFeedback();

        return back();
    }

    public function update($user_id, UpdateUserRequest $request)
    {
        $this->authorize('edit', User::class);
        $user = $this->user_repo->findById($user_id);

        if($request->hasFile('image'))
        {
            $request->request->add(['image_id'=> MediaFileService::publicUpload($request->file('image'))->id]);

            if (isset($user->image))
            {
                $user->image->delete();
            }

        }else {
            $request->request->add(['image_id'=> $user->image_id]);
        }

        $this->user_repo->update($user_id, $request);

        newFeedback();

        return redirect()->back();

    }

    public function destroy($user_id)
    {
        $this->authorize('edit', User::class);
        $user = $this->user_repo->findById($user_id);

        $user->delete();

        return AjaxResponses::success();
    }

    public function manualVerify($user_id)
    {
        $this->authorize('manualVerify', User::class);
        $user = $this->user_repo->findById($user_id);

        if (! $user->hasVerifiedEmail())
        {
            $user->markEmailAsVerified();
            return AjaxResponses::success();
        }

        return AjaxResponses::success($body="ایمیل کاربر تایید داشته است");
    }

    public function addRole(AddRoleRequest $request, User $user)
    {
        $trans_role = __($request->role);

        $this->authorize('addRole', User::class);
        $user->assignRole($request->role);
        newFeedback("موفقیت آمیز","به کاربر {$user->name} نقش کاربری {$trans_role} داده شد.","success");
        return back();
    }

    public function removeRole($user_id, $role)
    {
        $this->authorize('removeRole', User::class);
        $user = $this->user_repo->findById($user_id);
        $user->removeRole($role);
        return AjaxResponses::success("عملیات حذف موفقیت آمیز بود");
    }
}
