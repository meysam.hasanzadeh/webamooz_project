@extends('Dashboard::master')
@section('breadcrumb')
    <li><a href="{{ route('courses.details', $season->course_id) }}" title="سرفصل">{{ $season->course->title }}</a></li>
    <li><a title="ویرایش دوره">ویرایش سرفصل</a></li>
@endsection
@section('content')
    <div class="row no-gutters">
        <div class="col-12 bg-white">
            <p class="box__title">بروزرسانی سرفصل</p>
            <form action="{{ route('seasons.update', $season->id) }}" class="padding-30" method="post"
                enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                <x-input type="text" placeholder="عنوان سرفصل" class="text mb-4" value="{{ $season->title }}" name="title" required/>
                <x-input type="text" placeholder="شماره سرفصل" class="text mb-4" value="{{ $season->number }}" name="number"/>
                <div>
                    <button class="btn btn-webamooz_net">بروزرسانی سرفصل</button>
                </div>
            </form>
        </div>
    </div>
@endsection
