<?php


namespace Mdh\Course\Models;


use Illuminate\Database\Eloquent\Model;
use Mdh\Category\Models\Category;
use Mdh\Course\Repositories\CourseRepo;
use Mdh\Media\Models\Media;
use Mdh\Payment\Models\Payment;
use Mdh\User\Models\User;

class Course extends Model
{
    protected $guarded = [];
    const TYPE_FREEE = 'free';
    const TYPE_CASH = 'cash';
    public static $types = [self::TYPE_FREEE, self::TYPE_CASH];

    const STATUS_COMPLETED = 'completed';
    const STATUS_NOT_COMPLETED = 'not-completed';
    const STATUS_LOCKED = 'locked';
    public static $statuses = [self::STATUS_COMPLETED,self::STATUS_NOT_COMPLETED,self::STATUS_LOCKED];

    const CONFIRMATION_STATUS_ACCEPTED = 'accepted';
    const CONFIRMATION_STATUS_REJECTED = 'rejected';
    const CONFIRMATION_STATUS_PENDING = 'pending';

    public static $confirmationStatuses =
        [self::CONFIRMATION_STATUS_ACCEPTED,self::CONFIRMATION_STATUS_PENDING,self::CONFIRMATION_STATUS_PENDING];

    public function banner()
    {
        return $this->belongsTo(Media::class, 'banner_id');
    }

    public function teacher()
    {
        return $this->belongsTo(User::class, 'teacher_id');
    }

    public function students()
    {
        return $this->belongsToMany(User::class, 'course_user', 'course_id', 'user_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function seasons()
    {
        return $this->hasMany(Season::class);
    }

    public function lessons()
    {
        return $this->hasMany(Lesson::class);
    }

    public function getDuration()
    {
        return (new CourseRepo())->getDuration($this->id);
    }

    public function formattedDuration()
    {
        return gmdate('H:i:s', $this->getDuration() * 60);
    }

    public function getFormatedPrice()
    {
        return number_format($this->price);
    }

    public function getDiscountPercent()
    {
        // todo add method
        return 0;
    }

    public function getDiscountAmount()
    {
        // todo add method
        return 0;
    }

    public function getFinalPrice()
    {
        return $this->price - $this->getDiscountAmount();
    }

    public function getFormattedFinalPrice()
    {
        return number_format($this->price - $this->getDiscountAmount());
    }

    public function path()
    {
        return route('singleCourse',$this->id.'-'.$this->slug);
    }

    public function lessonCount()
    {
        return (new CourseRepo())->getLessonCount($this->id);
    }

    public function shortUrl()
    {
        return route('singleCourse',$this->id);
    }

    public function payments()
    {
        return $this->morphMany(Payment::class,'paymentable');
    }

    public function hasStudent($student_id)
    {
        return resolve(CourseRepo::class)->hasStudent($this,$student_id);
    }

    public function downloadLinks():array
    {
        $links = [];

        foreach (resolve(CourseRepo::class)->getLessons($this->id) as $lesson)
        {
            $links[] = $lesson->downloadLink();
        }

        return $links;
    }
}
