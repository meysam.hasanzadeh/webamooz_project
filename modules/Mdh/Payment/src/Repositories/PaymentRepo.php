<?php


namespace Mdh\Payment\Repositories;


use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Mdh\Payment\Models\Payment;
use phpDocumentor\Reflection\Types\Collection;
use Ramsey\Uuid\Type\Integer;

class PaymentRepo
{
    protected $query;

    public function __construct()
    {
        $this->query = Payment::query();
    }

    public function store($data)
    {
        return Payment::create([
            'buyer_id'=> $data['buyer_id'],
            'paymentable_id'=> $data['paymentable_id'],
            'paymentable_type'=> $data['paymentable_type'],
            'amount'=> $data['amount'],
            'invoice_id'=> $data['invoice_id'],
            'gateway'=> $data['gateway'],
            'status'=> $data['status'],
            'seller_p'=> $data['seller_p'],
            'seller_share'=> $data['seller_share'],
            'site_share'=> $data['site_share'],
        ]);
    }

    public function findByInvoiceId($invoiceId)
    {
        return Payment::where('invoice_id',$invoiceId)->first();
    }

    public function changeStatus($paymentId, $status)
    {
        return Payment::where('id',$paymentId)->update([
            'status'=> $status,
        ]);
    }

    public function searchByEmail($email)
    {
        if (!is_null($email))
        {
            $this->query->join('users','payments.buyer_id','users.id')
                ->select("payments.*","users.id")
                ->where('email',"like", "%".$email."%");
        }

        return $this;
    }

    public function searchByAmount($amount)
    {
        if (!is_null($amount))
        {
            $this->query->where('amount',$amount);
        }

        return $this;
    }

    public function searchByInvoiceId($invoice_id)
    {
        if (!is_null($invoice_id))
        {
            $this->query->where('invoice_id',"like","%".$invoice_id."%");
        }

        return $this;
    }

    public function searchAfterDate($date)
    {
        if (!is_null($date))
        {
            $this->query->whereDate('created_at',">=",$date);
        }

        return $this;
    }

    public function searchBeforeDate($date)
    {
        if (!is_null($date))
        {
            $this->query->whereDate('created_at',"<=",$date);
        }

        return $this;
    }

    public function paginate()
    {
        return $this->query->latest()->paginate();
    }

    public function getLastNDaysPayments($status,$days=null)
    {
        $query = Payment::query();

        if (!is_null($days)) $query->where('created_at',">=", now()->addDays($days));
        return $query->where('status',$status)->latest();
    }

    public function getLastNDaysSuccessPayments($days=null)
    {
        return $this->getLastNDaysPayments(Payment::STATUS_SUCCESS, $days);
    }

    public function getLastNDaysTotal($days=null)
    {
        return $this->getLastNDaysSuccessPayments($days)->sum('amount');
    }

    public function getLastNDaysSiteBenefit($days=null)
    {
        return $this->getLastNDaysSuccessPayments($days)->sum('site_share');
    }

    public function getLastNDaysSellerShare($days=null)
    {
        return $this->getLastNDaysSuccessPayments($days)->sum('seller_share');
    }

    public function getDayPayments($day,$status)
    {
        return Payment::query()
            ->where('status',$status)
            ->whereDate('created_at',$day)->latest();
    }

    public function getDaySuccessPayments($day)
    {
        return $this->getDayPayments($day,Payment::STATUS_SUCCESS);
    }

    public function getDayFailedPayments($day)
    {
        return $this->getDayPayments($day,Payment::STATUS_FAIL);
    }

    public function getDaySuccessPaymentsTotal($day)
    {
        return $this->getDaySuccessPayments($day)->sum('amount');
    }

    public function getDayFailedPaymentsTotal($day)
    {
        return $this->getDayFailedPayments($day)->sum('amount');
    }

    public function getDaySiteShare($day)
    {
        return $this->getDaySuccessPayments($day)->sum('site_share');
    }

    public function getDaySellerShare($day)
    {
        return $this->getDaySuccessPayments($day)->sum('seller_share');
    }

    public function getDailySummery($dates)
    {
        return Payment::where('created_at',">=",$dates->keys()->first())
            ->groupBy('date')
            ->orderBy('date')
            ->get([
                DB::raw("DATE(created_at) as date"),
                DB::raw("SUM(amount) as totalAmount"),
                DB::raw("SUM(seller_share) as totalSellerShare"),
                DB::raw("SUM(site_share) as totalSiteShare"),
            ]);
    }
}
