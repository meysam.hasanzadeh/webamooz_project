<?php


namespace Mdh\RolePermissions\Http\Controllers;


use App\Http\Controllers\Controller;
use Mdh\Common\Responses\AjaxResponses;
use Mdh\RolePermissions\Http\Requests\RoleUpdateRequest;
use Mdh\RolePermissions\Models\Role;
use Mdh\RolePermissions\Repositories\PermissionRepo;
use Mdh\RolePermissions\Repositories\RoleRepo;
use Mdh\RolePermissions\Http\Requests\RoleRequest;


class RolePermissionsController extends Controller
{
    private RoleRepo $role_repo;
    private PermissionRepo $permission_repo;

    public function __construct(RoleRepo $role_repo, PermissionRepo $permission_repo)
    {
        $this->role_repo = $role_repo;
        $this->permission_repo = $permission_repo;
    }

    public function index()
    {
        $this->authorize('index', Role::class);
        $roles = $this->role_repo->all();
        $permissions = $this->permission_repo->all();

        return view('RolePermissions::index', compact('roles', 'permissions'));
    }

    public function edit($id)
    {
        $role = $this->role_repo->findById($id);
        $permissions = $this->permission_repo->all();
        $this->authorize('edit', Role::class);

        return view('RolePermissions::edit', compact('role', 'permissions'));
    }

    public function store(RoleRequest $request)
    {
        $this->authorize('create', Role::class);
        $this->role_repo->create($request);

        return redirect(route('role-permissions.index'));
    }


    public function update(RoleUpdateRequest $request, $roleId)
    {
        $this->authorize('edit', Role::class);
        $this->role_repo->update($roleId, $request);
        return redirect(route('role-permissions.index'));
    }

    public function destroy($id)
    {
        $this->role_repo->delete($id);

        return AjaxResponses::success();
    }
}
