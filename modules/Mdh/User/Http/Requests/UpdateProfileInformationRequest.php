<?php


namespace Mdh\User\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;
use Mdh\RolePermissions\Models\Permission;
use Mdh\User\Rules\ValidPassword;


class UpdateProfileInformationRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }


    public function rules()
    {
        $rule = [
            'name' => ['required','min:3','max:190'],
            'email' => ['required','min:3','max:190','unique:users,email,'.auth()->id()],
            'username' => ['nullable','min:3','max:190','unique:users,username,'.auth()->id()],
            'mobile' => ['required','numeric'],
            'password'=> ['nullable',new ValidPassword()]
        ];

        if (auth()->user()->hasPermissionTo(Permission::PERMISSION_TEACH))
        {
            $rule['username'] = ['required','min:3','max:190','unique:users,username,'.auth()->id()];
            $rule += [
                'card_number'=> ['required','size:16','string'],
                'shaba'=> ['required','size:24']
            ];
        }
        return $rule;
    }

    public function attributes()
    {
        return [
            'name'=>'نام',
            'email'=>'ایمیل',
            'username'=>'نام کاربری',
            'mobile'=>'موبایل',
            'password'=> 'رمز عبور',
            'card_number'=> 'شماره کارت',
            'shaba'=> 'شماره شبا'
        ];
    }
}
