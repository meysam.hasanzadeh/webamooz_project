<?php

namespace Mdh\Category\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=> ['required','max:190','string'],
            'slug'=> ['string','required','max:190'],
            'parent_id'=> ['nullable','exists:categories,id']
        ];
    }
}
