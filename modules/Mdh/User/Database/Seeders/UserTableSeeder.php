<?php

namespace Mdh\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Mdh\User\Models\User;


class UserTableSeeder extends Seeder
{
    public function run()
    {
        foreach (User::$defaultUsers as $user){
            User::firstOrCreate(
                ['email' => $user['email']],
                ['name'=> $user['name'],
                'email'=> $user['email'],
                'password'=> bcrypt($user['password'])
                ])->assignRole($user['role'])->markEmailAsVerified();
        }
    }
}
