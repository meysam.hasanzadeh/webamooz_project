<?php

namespace Mdh\Course\Http\Controllers;

use App\Http\Controllers\Controller;
use Mdh\Common\Responses\AjaxResponses;
use Mdh\Course\Http\Requests\SeasonRequest;
use Mdh\Course\Models\Season;
use Mdh\Course\Repositories\CourseRepo;
use Mdh\Course\Repositories\SeasonRepo;


class SeasonController extends Controller
{

    private SeasonRepo $seasonRepo;

    public function __construct(SeasonRepo $seasonRepo)
    {
        $this->seasonRepo = $seasonRepo;
    }

    public function store($course_id, SeasonRequest $request, CourseRepo $courseRepo)
    {
        $this->authorize('createSeason', $courseRepo->findById($course_id));
        $this->seasonRepo->store($course_id, $request);

        newFeedback();

        return back();
    }

    public function edit($season_id)
    {
        $season = $this->seasonRepo->findById($season_id);
        $this->authorize('edit', $season);

        return view('Courses::seasons.edit', compact('season'));
    }

    public function update($season_id, SeasonRequest $request)
    {
        $this->seasonRepo->update($season_id, $request);

        newFeedback();
        return redirect()->back();
    }

    public function destroy($season_id)
    {
        $season = $this->seasonRepo->findById($season_id);

        $this->authorize('delete', $season);
        $season->delete();

        return AjaxResponses::success();
    }

    public function accept($id)
    {
        $this->authorize('change_confirmation_status', Season::class);
        if($this->seasonRepo->updateConfirmationStatus($id, Season::CONFIRMATION_STATUS_ACCEPTED))
        {
            return AjaxResponses::success();
        }

        return AjaxResponses::FailedResponse();
    }

    public function reject($id)
    {
        $this->authorize('change_confirmation_status', Season::class);
        if($this->seasonRepo->updateConfirmationStatus($id, Season::CONFIRMATION_STATUS_REJECTED))
        {
            return AjaxResponses::success();
        }

        return AjaxResponses::FailedResponse();
    }

    public function lock($id)
    {
        $this->authorize('change_confirmation_status', Season::class);
        if($this->seasonRepo->updateStatus($id, Season::STATUS_LOCKED))
        {
            return AjaxResponses::success();
        }

        return AjaxResponses::FailedResponse();
    }

    public function unlock($id)
    {
        $this->authorize('change_confirmation_status', Season::class);
        if($this->seasonRepo->updateStatus($id, Season::STATUS_OPENED))
        {
            return AjaxResponses::success();
        }

        return AjaxResponses::FailedResponse();
    }
}
