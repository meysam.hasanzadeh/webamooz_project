<?php


use Illuminate\Support\Facades\Route;

Route::group([], function ($router){
    $router->any('payments/callback','PaymentController@callback')->name('payments.callback');
    $router->get('payments',  [
        "uses"=> 'PaymentController@index',
        "as" => 'payments.index'
    ]);
    $router->get('payments/my_purchases',  [
        "uses"=> 'PaymentController@myPurchases',
        "as" => 'payments.my_purchases'
    ]);
});
