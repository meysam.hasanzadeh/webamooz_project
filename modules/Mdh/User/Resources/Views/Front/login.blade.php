@extends('User::Front.master')

@section('page_name')
    صفحه ورود
@endsection

@section('content')

    <form action="{{ route('login') }}" class="form" method="post">
        @csrf
        <a class="account-logo" href="/">
            <img src="img/weblogo.png" alt="">
        </a>
        <div class="form-content form-account">
            <input type="text" id="email" name="email" class="txt-l txt @error('password') is-invalid @enderror"
                   placeholder="ایمیل یا شماره موبایل" required autocomplete="field"
            >
            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror

            <input id="password" type="password" class="txt txt-l @error('password') is-invalid @enderror"
                   name="password" required autocomplete="new-password" placeholder="رمز عبور"
            >
            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
            <br>
            <button class="btn btn--login">ورود</button>
            <label class="ui-checkbox">
                مرا بخاطر داشته باش
                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                <span class="checkmark"></span>
            </label>
            <div class="recover-password">
                <a href="{{ route('password.request') }}">بازیابی رمز عبور</a>
            </div>
            <div class="recover-password">
                <a href="/">صفحه اصلی</a>
            </div>
        </div>
        <div class="form-footer">
            <a href="{{ 'register' }}">صفحه ثبت نام</a>
        </div>
    </form>

@endsection;
