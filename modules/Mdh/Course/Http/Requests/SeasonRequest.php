<?php


namespace Mdh\Course\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Mdh\Course\Models\Course;
use Mdh\Course\Rules\ValidTeacher;


class SeasonRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }


    public function rules()
    {
        return [
            'title' => ['required','min:3','max:190'],
            'number' => ['nullable','min:0','max:250','numeric'],
        ];

    }

    public function attributes()
    {
        return [
            'title'=>'عنوان فصل',
            'number'=>'شماره فصل',
        ];
    }
}
