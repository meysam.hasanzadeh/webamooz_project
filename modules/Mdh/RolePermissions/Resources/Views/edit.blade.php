@extends('Dashboard::master')


@section('breadcrumb')
    <li><a class="is-active" href="{{ route('role-permissions.index') }}">نقش های کاربری</a></li>
    <li><a class="is-active">ویرایش</a></li>
@endsection


@section('content')
    <div class="row no-gutters">
        <div class="col-8 bg-white">
            <p class="box__title">ایجاد نقش کاربری جدید</p>
            <form action="{{ route('role-permissions.update', $role->id) }}" method="post" class="padding-30">
                @csrf
                @method('patch')
                <input type="hidden" name="id" value="{{ $role->id }}">
                <input type="text" name="name" required placeholder="نام مجوز"
                       value="{{ $role->name }}" class="text @error('name') is-invalid @enderror">
                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror

                <p class="box__title margin-bottom-15">انتخاب مجوزها</p>
                @foreach($permissions as $permission)
                    <label class="ui-checkbox padding-8-20">
                        <input type="checkbox" name="permissions[{{$permission->name}}]" class="sub-checkbox"
                               data-id="1" value="{{ $permission->name }}"
                               @if($role->hasPermissionTo($permission->id)) checked @endif
                        >
                        <span class="checkmark"></span>
                        <p class="margin-15">@lang($permission->name)</p>
                    </label>
                @endforeach
                @error('permissions')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
                <br>
                <button class="btn btn-webamooz_net">بروزرسانی</button>
            </form>
        </div>
    </div>
@endsection
