<?php


namespace Mdh\Media\Models;


use Illuminate\Database\Eloquent\Model;
use Mdh\Media\Services\MediaFileService;

class Media extends Model
{
    protected $guarded = [];

    protected $casts = [
        'files'=>'json'
    ];

    protected static function booted()
    {
        static::deleting(function ($media)
        {
            MediaFileService::delete($media);
        });
    }

    public function getThumbAttribute()
    {
        return MediaFileService::thumb($this);
    }
}
