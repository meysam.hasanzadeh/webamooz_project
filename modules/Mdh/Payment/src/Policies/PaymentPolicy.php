<?php

namespace Mdh\Payment\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Mdh\Course\Repositories\CourseRepo;
use Mdh\RolePermissions\Models\Permission;
use Mdh\User\Models\User;

class PaymentPolicy
{
    use HandlesAuthorization;

    public function manage($user)
    {
        if($user->hasPermissionTo(Permission::PERMISSION_MANAGE_PAYMENTS)) return true;

        return null;
    }
}
