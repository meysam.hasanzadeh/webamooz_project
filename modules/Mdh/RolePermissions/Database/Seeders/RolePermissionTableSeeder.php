<?php

namespace Mdh\RolePermissions\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolePermissionTableSeeder extends Seeder
{
    public function run()
    {
        foreach (\Mdh\RolePermissions\Models\Permission::$permissions as $permission){
            Permission::FindOrCreate($permission);
        }

        foreach (\Mdh\RolePermissions\Models\Role::$roles as $role => $permissions){
            Role::FindOrCreate($role)->givePermissionTo($permissions);
        }
    }
}
