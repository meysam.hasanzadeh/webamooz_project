<div class="episodes-list">
    <div class="episodes-list--title">
        <span>فهرست جلسات</span>
        @can('download',$course)
            <a href="{{ route('courses.downloadLinks', $course) }}" class="detail-download d-flex font-size-13">
                <span class="margin-left-8">دریافت همه لینک های دانلود</span>
                <i class="icon-download font-size-13"></i>
            </a>
        @endcan
    </div>
    <div class="episodes-list-section">
        @foreach($lessons as $lesson)
            <div class="episodes-list-item {{auth()->check() && auth()->user()->can('download',$lesson) ? '' : 'lock' }}">
                <div class="section-right">
                    <span class="episodes-list-number">{{ $lesson->id }}</span>
                    <div class="episodes-list-title">
                        <a href="{{ $lesson->path() }}">{{ $lesson->title }}</a>
                    </div>
                </div>
                <div class="section-left">
                    <div class="episodes-list-details">
                        <div class="episodes-list-details">
                            <span class="detail-type">
                                {{auth()->check() && auth()->user()->can('download',$lesson) ? 'رایگان' : '' }}
                            </span>
                            <span class="detail-time">{{ $lesson->time }}</span>
                            <a class="detail-download"
                                @can('download',$lesson) href="{{ $lesson->downloadLink() }}" @endcan
                            >
                                <i class="icon-download"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
