<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware'=> ['web'], 'namespace'=> 'Mdh\Front\Http\Controllers'], function ($router)
{
   $router->get('/', 'FrontController@index');
   $router->get('/c-{slug}', 'FrontController@singleCourse')->name('singleCourse');
   $router->get('/tutors/{username}', 'FrontController@singleTutor')->name('singleTutor');
});
