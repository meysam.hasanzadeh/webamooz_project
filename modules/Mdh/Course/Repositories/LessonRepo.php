<?php


namespace Mdh\Course\Repositories;

use Illuminate\Support\Str;
use Mdh\Course\Models\Lesson;


class LessonRepo
{
    public function all()
    {
        return Lesson::all();
    }

    public function store($values, $courseId)
    {
        return Lesson::query()->create([
            'title'=> $values->title,
            'slug'=> $values->slug ? Str::slug($values->slug) : Str::slug($values->title),
            'number'=> $this->generateNumber($courseId, $values->number),
            'season_id' => $values->season_id,
            'media_id' => $values->media_id,
            'course_id' => $courseId,
            'user_id' => auth()->id(),
            'time'=> $values->time,
            'is_free'=> $values->is_free,
            'confirmation_status'=> Lesson::CONFIRMATION_STATUS_PENDING,
            'status'=> Lesson::STATUS_OPENED,
            'body'=> $values->body
        ]);
    }

    public function getLessonCourse($course_id)
    {
        return Lesson::where('course_id', $course_id)
            ->orderBy('number')
            ->paginate();
    }

    public function getAcceptedLessons(int $courseId)
    {
        return Lesson::where('confirmation_status', Lesson::CONFIRMATION_STATUS_ACCEPTED)
            ->where('course_id',$courseId)
            ->get();
    }

    public function getFirstLesson($courseId)
    {
        return Lesson::where('course_id', $courseId)
            ->where('confirmation_status', Lesson::CONFIRMATION_STATUS_ACCEPTED)
            ->orderBy('number', 'asc')
            ->first();
    }

    public function paginate($courseId)
    {
        return Lesson::where('course_id', $courseId)->paginate();
    }

    public function findById($id)
    {
        return Lesson::findOrFail($id);
    }

    public function acceptAll($courseId)
    {
        return Lesson::where('course_id', $courseId)->update([
           'confirmation_status'=> Lesson::CONFIRMATION_STATUS_ACCEPTED,
        ]);
    }

    public function allExceptByID($id)
    {
        return $this->all()->filter(function ($item) use($id) {
             return $item->id != $id;
        });
    }

    public function update($id, $courseId, $values)
    {
        Lesson::where('id', $id)->update([
            'title'=> $values->title,
            'slug'=> $values->slug ? Str::slug($values->slug) : Str::slug($values->title),
            'number'=> $this->generateNumber($courseId, $values->number),
            'season_id' => $values->season_id,
            'media_id' => $values->media_id,
            'time'=> $values->time,
            'is_free'=> $values->is_free,
            'body'=> $values->body
        ]);
    }

    public function delete($id)
    {
        return Lesson::where('id',$id)->delete();
    }

    public function updateConfirmationStatus($id, $status)
    {
        if (is_array($id))
        {
            Lesson::query()->whereIn('id',$id)->update([
                'confirmation_status'=> $status
            ]);
        }
        return Lesson::where('id',$id)->update([
           'confirmation_status'=> $status
        ]);
    }

    public function updateStatus($id, $status)
    {
        return Lesson::where('id',$id)->update([
            'status'=> $status
        ]);
    }

    public function generateNumber($courseId, $number): int
    {
        $course_repo = new CourseRepo();

        if (is_null($number)) {
            $number = $course_repo->findById($courseId)->lessons()->orderBy('number', 'desc')->firstOrNew([])->number ?: 0;
            $number++;
        }
        return $number;
    }

    public function getLesson(int $courseId, int $lessonId)
    {
        return Lesson::where('course_id', $courseId)->where('id', $lessonId)->first();
    }

}
