<?php
 return [
   "mediaTypeService"=> [
       "image"=> [
           "extensions"=> ['jpg','png','jpeg'],
           "handler" => \Mdh\Media\Services\ImageFileService::class
       ],
       "video"=> [
           "extensions"=> ['avi','mp4','mkv'],
           "handler" => \Mdh\Media\Services\VideoFileService::class
       ],
       "zip"=> [
           "extensions"=> ['avi','mp4','mkv'],
           "handler" => \Mdh\Media\Services\ZipFileService::class
       ],
   ]
 ];
