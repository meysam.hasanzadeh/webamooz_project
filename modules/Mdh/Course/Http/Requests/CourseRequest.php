<?php


namespace Mdh\Course\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Mdh\Course\Models\Course;
use Mdh\Course\Rules\ValidTeacher;


class CourseRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }


    public function rules()
    {
        $rules = [
            'title' => ['required','min:3','max:190'],
            'slug' => ['required','min:3','max:190','unique:courses,slug'],
            'priority' => ['nullable','numeric'],
            'price' => ['required','numeric','min:0','max:10000000'],
            'percent' => ['required','numeric','min:0','max:100'],
            'teacher_id' => ['required','exists:users,id',new ValidTeacher()],
            'type' => ['required',Rule::in(Course::$types)],
            'status' => ['required',Rule::in(Course::$statuses)],
            'category_id' => ['required','exists:categories,id'],
            'image' => ['required','mimes:jpg,png,jpeg'],
        ];

        if (request()->method === 'PATCH'){
            $rules['image']= ['nullable','mimes:jpg,png,jpeg'];
            $rules['slug'] = ['required','min:3','max:190','unique:courses,slug,'.request()->route('course')];
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'slug'=>'نام انگلیسی',
            'priority'=>'ترتیب دوره',
            'price'=>'قیمت',
            'percent'=>'درصد',
            'teacher_id'=>'مدرس',
            'type'=>'نوع',
            'status'=>'وضعیت',
            'category_id'=>'دسته بندی',
            'image'=>'بنر دوره',
        ];
    }

    public function messages()
    {
        return [
//            'price.min' => trans('Courses::validation.price_min'),
        ];
    }
}
