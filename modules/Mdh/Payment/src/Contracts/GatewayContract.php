<?php

namespace Mdh\Payment\Contracts;

use Illuminate\Http\Request;
use Mdh\Payment\Models\Payment;

interface GatewayContract
{
    public function request($amount,$description);

    public function verify(Payment $payment);

    public function redirect();

    public function getName();
}
