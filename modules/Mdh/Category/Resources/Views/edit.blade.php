@extends('Dashboard::master')


@section('breadcrumb')
    <li><a class="is-active" href="{{ route('categories.index') }}">دسته بندی ها</a></li>
    <li><a class="is-active">ویرایش</a></li>
@endsection


@section('content')
    <div class="row no-gutters">
        <div class="col-8 bg-white">
            <p class="box__title">ایجاد دسته بندی جدید</p>
            <form action="{{ route('categories.update', $category->id) }}" method="post" class="padding-30">
                @csrf
                @method('put')
                <input type="text" name="title" required placeholder="نام دسته بندی"
                       value="{{ $category->title }}" class="text mb-4">
                <input type="text" name="slug" required placeholder="نام انگلیسی دسته بندی"
                       value="{{ $category->slug }}" class="text"
                >
                <p class="box__title margin-bottom-15">انتخاب دسته پدر</p>
                <select name="parent_id" id="parent_id">
                    <option value="">دسته پدر ندارد</option>
                    @foreach($categories as $categoryItem)
                        <option value="{{ $categoryItem->id }}" @if($categoryItem->id == $category->parent_id) selected
                            @endif>{{ $categoryItem->title }}
                        </option>
                    @endforeach
                </select>
                <button class="btn btn-webamooz_net">بروز رسانی</button>
            </form>
        </div>
    </div>
@endsection

{{--@section('js')--}}
{{--    <script src="/panel/js/tagsInput.js"></script>--}}
{{--@endsection--}}
