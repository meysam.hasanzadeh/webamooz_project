<?php


namespace Mdh\Front\Providers;


use Illuminate\Support\ServiceProvider;
use Mdh\Category\Repositories\CategoryRepo;
use Mdh\Course\Repositories\CourseRepo;

class FrontServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->loadRoutesFrom(__DIR__ . '/../Routes/front-routes.php');
        $this->loadViewsFrom(__DIR__ . '/../Resources/Views', 'Front');

        view()->composer('Front::layouts.header', function ($view)
        {
            $categories = (new CategoryRepo())->tree();

            $view->with(compact('categories'));
        });

        view()->composer('Front::layouts.latest-courses', function ($view)
        {
            $latestCourses = (new CourseRepo())->latestCourses();

            $view->with(compact('latestCourses'));
        });
    }
}
