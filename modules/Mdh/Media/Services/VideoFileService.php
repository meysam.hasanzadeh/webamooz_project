<?php


namespace Mdh\Media\Services;


use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Mdh\Media\Contracts\FileServiceContract;
use Mdh\Media\Models\Media;

class VideoFileService extends DefaultFileService implements FileServiceContract
{
    public static function upload(UploadedFile $file, string $fileName, string $dir): array
    {
        $filename = uniqid();
        $extension = $file->getClientOriginalExtension();
        Storage::putFileAs($dir, $file, $filename .'.'. $extension);

        return ["video"=> $filename.'.'.$extension];
    }

    public static function thumb(Media $media)
    {
        return url("img/video-thumb.png");
    }

    static function getFileName()
    {
        return (static::$media->is_private ? 'private/' : 'public/').static::$media->files['video'];
    }
}
