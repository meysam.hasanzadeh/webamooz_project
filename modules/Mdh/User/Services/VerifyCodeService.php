<?php
namespace Mdh\User\Services;

use Psr\SimpleCache\InvalidArgumentException;

class VerifyCodeService
{
    private static $min = 100000;
    private static $max = 999999;
    private static $prefix = 'verify_code_';

    public static function generate()
    {
        return random_int(self::$min, self::$max);
    }

    public static function store($id, $code, $time)
    {
        cache()->set(
            'verify_code_'.$id,
            $code,
            $time
        );
    }

    public static function get($id)
    {
        return cache()->has(self::$prefix.$id);
    }

    public static function checkCache($id)
    {
        return self::get(self::$prefix.$id);
    }

    public static function delete($id)
    {
        cache()->delete(self::$prefix.$id);
    }

    public static function getRule()
    {
        return [
            'required','numeric','between:'.self::$min.','.self::$max
        ];
    }

    public static function check($code, $id)
    {
        if ($code != self::get($id)) return false;

        self::delete($id);
        return true;
    }
}
