<?php


namespace Mdh\Course\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;
use Mdh\Media\Models\Media;
use Mdh\User\Models\User;

class Lesson extends Model
{
    protected $guarded = [];

    const STATUS_OPENED = 'opened';
    const STATUS_LOCKED = 'locked';
    public static array $statuses = [self::STATUS_OPENED, self::STATUS_LOCKED];


    const CONFIRMATION_STATUS_ACCEPTED = 'accepted';
    const CONFIRMATION_STATUS_REJECTED = 'rejected';
    const CONFIRMATION_STATUS_PENDING = 'pending';

    public static array $confirmationStatuses = [
        self::CONFIRMATION_STATUS_ACCEPTED,
        self::CONFIRMATION_STATUS_PENDING,
        self::CONFIRMATION_STATUS_REJECTED
    ];

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function season()
    {
        return $this->belongsTo(Season::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function media()
    {
        return $this->belongsTo(Media::class);
    }

    const CHOICES = [
         'accepted'=> "text-success",
         'rejected'=> "text-error",
         'pending'=> "text-warning"
    ];

    public function getConfirmationStatusCssClass($confirmationStatus)
    {
        return self::CHOICES[$confirmationStatus];
    }

    public function path()
    {
        return $this->course->path().'?lesson=l-'.$this->id."-".$this->slug;
    }

    public function downloadLink()
    {
        if ($this->media_id)
            return URL::temporarySignedRoute('media.download', now()->addDay(), ['media'=> $this->media_id]);
    }
}
