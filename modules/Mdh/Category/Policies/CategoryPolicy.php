<?php

namespace Mdh\Category\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Mdh\RolePermissions\Models\Permission;
use Mdh\User\Models\User;

class CategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function manage(User $user)
    {
        return $user->hasPermissionTo(Permission::PERMISSION_MANAGE_CATEGORIES);
    }
}
