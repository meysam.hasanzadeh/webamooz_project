
@if(session()->has('feedback'))
    @foreach(session()->get('feedback') as $message)
        $.toast({
        heading: "{{ $message['title'] }}",
        text: "{{ $message['body'] }}",
        showHideTransition: 'slide',
        icon: "{{ $message['type'] }}"
        });
    @endforeach
@endif
