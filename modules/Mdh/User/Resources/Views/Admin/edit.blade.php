@extends('Dashboard::master')
@section('breadcrumb')
    <li><a href="{{ route('users.index') }}" title="کاربران">کاربران</a></li>
    <li><a title="ویرایش کاربر">ویرایش کاربر</a></li>
@endsection
@section('content')
    <div class="row no-gutters margin-bottom-20">
        <div class="col-12 bg-white">
            <p class="box__title">بروزرسانی کاربر</p>
            <form action="{{ route('users.update', $user->id) }}" class="padding-30" method="post"
                enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                <x-input type="text" classDiv="mb-4" value="{{ $user->name }}" class="text" name="name"
                         placeholder="نام کاربر" required
                />
                <x-input type="text" classDiv="mb-4" value="{{ $user->username }}" class="text" name="username"
                         placeholder="نام کاربری"
                />
                <x-input type="email" classDiv="mb-4" value="{{ $user->email }}" class="text text-left" name="email"
                         placeholder="ایمیل" required
                />
                <x-input type="mobile" classDiv="mb-4" value="{{ $user->mobile }}" class="text text-left" name="mobile"
                         placeholder="موبایل" required
                />


                <x-select name="status" class="margin-bottom-0">
                    <option value="">ندارد</option>
                    @foreach(\Mdh\User\Models\User::$statuses as $status)
                        <option value="{{ $status }}" @if($status == $user->status)
                        selected @endif>@lang($status)
                        </option>
                    @endforeach
                </x-select>

                <x-select name="role" class="margin-bottom-0">
                    <option value="">ندارد</option>
                    @foreach($roles as $role)
                        <option value="{{ $role->name }}" @if($user->hasRole($role->name))
                        selected @endif>@lang($role->name)
                        </option>
                    @endforeach
                </x-select>


                <x-file placeholder="آپلود بنر کاربر" name="image" :value="$user->image"/>
                <div class="d-flex multi-text mb-3">
                    <x-input type="password" classDiv="width-100 margin-left-20" value=""
                             class="text" name="password" placeholder="رمز عبور جدید"
                    />
                </div>
                <div>
                    <button class="btn btn-webamooz_net">بروزرسانی کاربر</button>
                </div>
            </form>
        </div>
    </div>
    <div class="row no-gutters">
        <div class="col-6 margin-left-10 margin-bottom-20">
            <p class="box__title">درحال یادگیری</p>
            <div class="table__box">
                <table class="table">
                    <thead role="rowgroup">
                    <tr role="row" class="title-row">
                        <th>شناسه</th>
                        <th>نام دوره</th>
                        <th>نام مدرس</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr role="row" class="">
                        <td><a href="">1</a></td>
                        <td><a href="">دوره لاراول</a></td>
                        <td><a href="">صیاد اعظمی</a></td>
                    </tr>
                    <tr role="row" class="">
                        <td><a href="">1</a></td>
                        <td><a href="">دوره لاراول</a></td>
                        <td><a href="">صیاد اعظمی</a></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-6 margin-bottom-20">
            <p class="box__title">دوره های مدرس</p>
            <div class="table__box">
                <table class="table">
                    <thead role="rowgroup">
                    <tr role="row" class="title-row">
                        <th>شناسه</th>
                        <th>نام دوره</th>
                        <th>نام مدرس</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($user->courses as $course)
                        @if($course->teacher->name == $user->name)
                        <tr role="row" class="">
                            <td><a href="">{{ $course->id }}</a></td>
                            <td><a href="">{{ $course->title }}</a></td>
                            <td><a href="">{{ $course->teacher->name }}</a></td>
                        </tr>
                        @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="/panel/js/tagsInput.js?v={{ uniqid() }}"></script>

    <script>
        @include('Common::feedback')
    </script>
@endsection
