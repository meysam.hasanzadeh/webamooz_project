<?php


namespace Mdh\User\Repositories;


use Mdh\RolePermissions\Models\Permission;
use Mdh\User\Models\User;

class UserRepo
{
    public function all()
    {
        return User::all();
    }
    public function findByEmail($email)
    {
        return User::query()->where('email',$email)->first();
    }

    public function findById($id)
    {
        return User::query()->where('id',$id)->firstOrFail();
    }

    public function paginate()
    {
        return User::query()->paginate();
    }

    public function getTeachers()
    {
        return User::permission('teach')->get();
    }

    public function update($user_id, $values)
    {
        $update =[
           'name'=> $values->name,
           'email'=> $values->email,
           'username'=> $values->username,
           'mobile'=> $values->mobile,
           'status'=> $values->status,
           'image_id'=> $values->image_id,
        ];

        if (! is_null($values->password))
        {
            $update['password'] = bcrypt($values->password);
        }

        $user = User::find($user_id);
        $user->syncRoles([]);
        if($values->role)
            $user->assignRole($values->role);


        return User::where('id',$user_id)->update($update);
    }

    public function updateProfile($request)
    {
        auth()->user()->name = $request->name;

        if (auth()->user()->email != $request->email)
        {
            auth()->user()->email = $request->email;
            auth()->user()->email_verified_at = null;
        }

        if (auth()->user()->hasPermissionTo(Permission::PERMISSION_TEACH))
        {
            auth()->user()->card_number = $request->card_number;
            auth()->user()->shaba = $request->shaba;
            auth()->user()->username = $request->username;
        }

        if (isset($request->password))
        {
            auth()->user()->password = bcrypt($request->password);
        }
        auth()->user()->save();
    }
}
