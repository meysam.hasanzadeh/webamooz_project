<?php

namespace Mdh\Course\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Mdh\Course\Repositories\CourseRepo;
use Mdh\RolePermissions\Models\Permission;
use Mdh\User\Models\User;

class CoursePolicy
{
    use HandlesAuthorization;

    public function __construct()
    {

    }

    public function manage($user)
    {
        if($user->hasPermissionTo(Permission::PERMISSION_MANAGE_COURSES)) return true;

        return null;
    }

    public function index($user)
    {
        if($user->hasPermissionTo(Permission::PERMISSION_MANAGE_COURSES)
            || $user->hasPermissionTo(Permission::PERMISSION_MANAGE_OWN_COURSES)) return true;

        return null;
    }

    public function create($user)
    {
        return $user->hasPermissionTo(Permission::PERMISSION_MANAGE_COURSES) ||
               $user->hasPermissionTo(Permission::PERMISSION_MANAGE_OWN_COURSES);
    }

    public function createLesson($user, $course)
    {
        if ($user->id == $course->teacher_id)
        {
            return $user->hasPermissionTo(Permission::PERMISSION_MANAGE_COURSES);
        }

        return $user->hasPermissionTo(Permission::PERMISSION_MANAGE_COURSES);
    }

    public function edit($user, $course)
    {
        if($user->hasPermissionTo(Permission::PERMISSION_MANAGE_COURSES)) return true;

        return $user->hasPermissionTo(Permission::PERMISSION_MANAGE_OWN_COURSES) && $course->teacher_id = $user->id;
    }

    public function delete($user)
    {
        if($user->hasPermissionTo(Permission::PERMISSION_MANAGE_COURSES)) return true;

        return null;

    }

    public function change_confirmation_status($user)
    {
        if($user->hasPermissionTo(Permission::PERMISSION_MANAGE_COURSES)) return true;

        return null;
    }

    public function details($user, $course)
    {
        if($user->hasPermissionTo(Permission::PERMISSION_MANAGE_COURSES)) return true;
        if($user->hasPermissionTo(Permission::PERMISSION_MANAGE_OWN_COURSES)
            &&
            $course->teacher_id == $user->id)
            return true;
    }

    public function createSeason($user, $course)
    {
        if ($user->hasPermissionTo(Permission::PERMISSION_MANAGE_COURSES))
        {
            return true;
        }
        if($user->hasPermissionTo(Permission::PERMISSION_MANAGE_OWN_COURSES) && $course->teacher_id == $user->id)
        {
            return true;
        }
    }

    public function download($user, $course)
    {
        if($user->hasPermissionTo(Permission::PERMISSION_MANAGE_COURSES) ||
            $user->id == $course->teacher_id || $course->hasStudent($user->id)
        ){
            return true;
        }

//        if($course->students->contains($this->id)
//        ){
//            return true;
//        }
        return false;
    }
}
