<?php

namespace Mdh\Category\Http\Controllers;

use App\Http\Controllers\Controller;
use Mdh\Category\Http\Requests\CategoryRequest;
use Mdh\Category\Models\Category;
use Mdh\Category\Repositories\CategoryRepo;
use Mdh\Common\Responses\AjaxResponses;


class CategoryController extends Controller
{
    public $repo;

    public function __construct(CategoryRepo $categoryRepo)
    {
        $this->repo = $categoryRepo;
    }

    public function index()
    {
        $this->authorize('manage', Category::class);
        $categories = $this->repo->all();

        return view('Category::index', compact('categories'));
    }

    public function store(CategoryRequest $request)
    {
        $this->authorize('manage', Category::class);
        $this->repo->store($request);
        return redirect()->back();
    }

    public function edit($categoryId)
    {
        $this->authorize('manage', Category::class);
        $category = $this->repo->findById($categoryId);
        $categories = $this->repo->allExceptByID($categoryId);

        return view('Category::edit', compact('category','categories'));
    }

    public function update($categoryId, CategoryRequest $request)
    {
        $this->authorize('manage', Category::class);
        $this->repo->update($categoryId, $request);
        return back();
    }

    public function destroy($categoryTd)
    {
        $this->authorize('manage', Category::class);
        $this->repo->delete($categoryTd);
        return AjaxResponses::success();
    }
}
