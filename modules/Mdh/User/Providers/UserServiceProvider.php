<?php


namespace Mdh\User\Providers;



use Database\Seeders\DatabaseSeeder;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Mdh\RolePermissions\Models\Permission;
use Mdh\User\Database\Seeders\UserTableSeeder;
use Mdh\User\Http\Middleware\StoreUserIp;
use Mdh\User\Models\User;
use Mdh\User\Policies\UserPolicy;

class UserServiceProvider extends ServiceProvider
{
    public function register()
    {
        config()->set('auth.providers.users.model', User::class);
        $this->loadRoutesFrom(__DIR__.'/../Routes/user_routes.php');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'User');
        $this->loadJsonTranslationsFrom(__DIR__.'/../Resources/Lang');
        $this->app['router']->pushMiddlewareToGroup('web', StoreUserIp::class);

        DatabaseSeeder::$seeders[] = UserTableSeeder::class;
        Gate::policy(User::class, UserPolicy::class);
    }

    public function boot()
    {
        config()->set('sidebar.items.users',[
            'icon'=> 'i-users',
            'title'=> 'کاربران',
            'url'=> route('users.index'),
            'permission'=> Permission::PERMISSION_MANAGE_USERS
        ]);

        config()->set('sidebar.items.profile',[
            'icon'=> 'i-user__inforamtion',
            'title'=> 'اطلاعات کاربری',
            'url'=> route('users.profile'),
        ]);
    }

}
