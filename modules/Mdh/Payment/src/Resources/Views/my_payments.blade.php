@extends('Dashboard::master')

@section('breadcrumb')
    <li><a class="is-active">پرداخت های من</a></li>
@endsection

@section('content')
    <div class="table__box">
        <table class="table">
            <thead>
            <tr class="title-row">
                <th>عنوان دوره</th>
                <th>تاریخ پرداخت</th>
                <th>مقدار پرداختی</th>
                <th>وضعیت پرداخت</th>
            </tr>
            </thead>
            <tbody>
                @foreach($myPurchases as $purchase)
                    <tr>
                        <td>
                            <a href="{{ $purchase->paymentable->path() }}" target="_blank">
                                {{ $purchase->paymentable->title }}
                            </a>
                        </td>
                        <td>{{ createFromCarbon($purchase->created_at) }}</td>
                        <td>{{ $purchase->paymentable->getFormatedPrice() }} تومان</td>
                        <td
                            class="
                                @if($purchase->status == \Mdh\Payment\Models\Payment::STATUS_SUCCESS)
                                    text-success
                                @else
                                    text-error
                                @endif">
                            @lang($purchase->status)
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="pagination">
        {{ $myPurchases->render() }}
    </div>
@endsection
