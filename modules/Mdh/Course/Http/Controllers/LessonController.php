<?php


namespace Mdh\Course\Http\Controllers;


use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Mdh\Common\Responses\AjaxResponses;
use Mdh\Course\Http\Requests\LessonRequest;
use Mdh\Course\Models\Course;
use Mdh\Course\Models\Lesson;
use Mdh\Course\Repositories\CourseRepo;
use Mdh\Course\Repositories\LessonRepo;
use Mdh\Course\Repositories\SeasonRepo;
use Mdh\Media\Services\MediaFileService;

class LessonController extends Controller
{
    private LessonRepo $lessonRepo;

    public function __construct(LessonRepo $lessonRepo)
    {
        $this->lessonRepo = $lessonRepo;
    }

    public function create($courseId, SeasonRepo $seasonRepo, CourseRepo $courseRepo)
    {
        $course = $courseRepo->findById($courseId);
        $this->authorize('createLesson', $course);
        $seasons = $seasonRepo->getSeasonCourse($courseId);

        return view('Courses::lessons.create', compact('seasons', 'course'));
    }

    public function store($courseId, LessonRequest $request, CourseRepo $courseRepo)
    {
        $course = $courseRepo->findById($courseId);
        $this->authorize('createLesson', $course);
        $request->request->add(['media_id'=> MediaFileService::privateUpload($request->file('lesson_file'))->id]);
        $this->lessonRepo->store($request, $courseId);
        newFeedback();
        return redirect()->route('courses.details', $courseId);
    }

    public function edit($courseId, $lessonId, CourseRepo $courseRepo)
    {
        $lesson = $this->lessonRepo->findById($lessonId);
        $this->authorize('edit', $lesson);
        $course = $courseRepo->findById($courseId);

        return view("Courses::lessons.edit", compact('course', 'lesson'));
    }

    public function update($courseId, $lessonId, LessonRequest $request)
    {
        $lesson = $this->lessonRepo->findById($lessonId);
        $this->authorize('edit', $lesson);

        if ($request->hasFile('lesson_file'))
        {
            if ($lesson->media)
            {
                $lesson->media->delete();
            }
            $request->request->add(['media_id'=> MediaFileService::privateUpload($request->file('lesson_file'))->id]);
        }else{
            $request->request->add(['media_id'=> $lesson->media_id]);
        }

        $this->lessonRepo->update($lessonId, $courseId, $request);
        newFeedback();
        return redirect(route('courses.details', $courseId));
    }

    public function destroy($courseId, $lessonId)
    {
        $lesson = $this->lessonRepo->findById($lessonId);
        $this->authorize('delete', $lesson);

        if ($lesson->media){
            $lesson->media->delete();
        }

        $this->lessonRepo->delete($lessonId);

        return AjaxResponses::success();
    }

    public function destroyMultiple($courseId, Request $request)
    {
        $ids = explode(',', $request->ids);
        foreach ($ids as $id)
        {
            $lesson = $this->lessonRepo->findById($id);
            $this->authorize('delete', $lesson);

            if ($lesson->media)
            {
                $lesson->media->delete();
            }

            $lesson->delete();
            newFeedback();
            return back();
        }
    }

    public function accept($lessonId)
    {
        $this->authorize('manage', Course::class);

        if($this->lessonRepo->updateConfirmationStatus($lessonId, Lesson::CONFIRMATION_STATUS_ACCEPTED))
        {
            return AjaxResponses::success();
        }
        return AjaxResponses::FailedResponse();
    }

    public function reject($lessonId)
    {
        $this->authorize('manage', Course::class);
        if($this->lessonRepo->updateConfirmationStatus($lessonId, Lesson::CONFIRMATION_STATUS_REJECTED))
        {
            return AjaxResponses::success();
        }

        return AjaxResponses::FailedResponse();
    }

    public function lock($lessonId)
    {
        $this->authorize('manage', Course::class);
        if($this->lessonRepo->updateStatus($lessonId, Lesson::STATUS_LOCKED))
        {
            return AjaxResponses::success();
        }

        return AjaxResponses::FailedResponse();
    }

    public function unlock($lessonId)
    {
        $this->authorize('manage', Course::class);
        $this->authorize('change_confirmation_status', Lesson::class);
        if($this->lessonRepo->updateStatus($lessonId, Lesson::STATUS_OPENED))
        {
            return AjaxResponses::success();
        }

        return AjaxResponses::FailedResponse();
    }

    public function acceptMultiple($courseId, Request $request)
    {
        $this->authorize('manage', Course::class);
        $ids = explode(',' ,$request->ids);
        foreach ($ids as $id)
        {
            $this->lessonRepo->updateConfirmationStatus($id, Lesson::CONFIRMATION_STATUS_ACCEPTED);
        }

        newFeedback();
        return back();
    }

    public function rejectMultiple($courseId, Request $request)
    {
        $this->authorize('manage', Course::class);
        $ids = explode(',' ,$request->ids);

        $this->lessonRepo->updateConfirmationStatus($ids, Lesson::CONFIRMATION_STATUS_REJECTED);
        newFeedback();
        return back();
    }

    public function acceptAll($courseId)
    {
        $this->authorize('manage', Course::class);
        $this->lessonRepo->acceptAll($courseId);
        newFeedback();
        return back();
    }

}
