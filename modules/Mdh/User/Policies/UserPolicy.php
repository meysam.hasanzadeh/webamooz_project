<?php

namespace Mdh\User\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Mdh\RolePermissions\Models\Permission;
use Mdh\User\Models\User;

class UserPolicy
{
    use HandlesAuthorization;

    public function index($user)
    {
        if($user->hasPermissionTo(Permission::PERMISSION_MANAGE_USERS)) return true;

        return null;
    }

    public function edit($user)
    {
        if($user->hasPermissionTo(Permission::PERMISSION_MANAGE_USERS)) return true;

        return null;
    }

    public function editProfile()
    {
        return auth()->check();
    }

    public function manualVerify($user)
    {
        if($user->hasPermissionTo(Permission::PERMISSION_MANAGE_USERS)) return true;

        return null;
    }

    public function addRole($user)
    {
        if($user->hasPermissionTo(Permission::PERMISSION_MANAGE_USERS)) return true;

        return null;
    }

    public function removeRole($user)
    {
        if($user->hasPermissionTo(Permission::PERMISSION_MANAGE_USERS)) return true;

        return null;
    }
}
