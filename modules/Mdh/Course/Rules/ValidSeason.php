<?php

namespace Mdh\Course\Rules;

use Illuminate\Contracts\Validation\Rule;
use Mdh\Course\Repositories\SeasonRepo;
use Mdh\User\Repositories\UserRepo;

class ValidSeason implements Rule
{
    public function __construct()
    {

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $season = resolve(SeasonRepo::class)->finByIdAndCourseId($value, request()->route('course'));

        if ($season)
        {
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "سرفصل انتخاب شده معتبر نمی باشد";
    }
}
