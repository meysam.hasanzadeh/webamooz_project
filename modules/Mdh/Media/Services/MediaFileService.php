<?php


namespace Mdh\Media\Services;


use Mdh\Media\Contracts\FileServiceContract;
use Mdh\Media\Models\Media;

class MediaFileService
{
    private static string $dir;
    private static $file;
    private static bool $isPrivate;

    public static function privateUpload($file)
    {
        self::$file = $file;
        self::$dir = "private/";
        self::$isPrivate = true;
        return self::upload();
    }

    public static function publicUpload($file)
    {
        self::$file = $file;
        self::$dir = "public/";
        self::$isPrivate = false;

        return self::upload();
    }

    private static function upload()
    {
        $extension = self::normalizeExtension(self::$file);

        foreach (config('mediaFile.mediaTypeService') as $type => $service)
        {
            if (in_array($extension,$service['extensions']))
            {
                return self::uploadByHandler(new $service['handler'], $type);
            }
        }
    }

    public static function stream(Media $media)
    {
        foreach (config('mediaFile.mediaTypeService') as $type => $service)
        {
            if ($media->type == $type)
            {
                return $service['handler']::stream($media);
            }
        }
    }

    public static function delete(Media $media)
    {
        foreach (config('mediaFile.mediaTypeService') as $type => $service)
        {
            if ($media->type == $type)
            {
                return $service['handler']::delete($media);
            }
        }

    }

    private static function normalizeExtension($file): string
    {
        return strtolower($file->getClientOriginalExtension());
    }

    private static function fileNameGenerator()
    {
        return uniqid();
    }

    private static function uploadByHandler(FileServiceContract $service, $type): Media
    {
        $media = new Media();
        $media->files = $service::upload(self::$file, self::fileNameGenerator(), self::$dir);
        $media->user_id = auth()->id();
        $media->type = $type;
        $media->filename = self::$file->getClientOriginalName();
        $media->is_private = self::$isPrivate;
        $media->save();
        return $media;
    }

    public static function thumb(Media $media)
    {
        foreach (config('mediaFile.mediaTypeService') as $type => $service)
        {
            if ($media->type == $type)
            {
                return $service['handler']::thumb($media);
            }
        }
    }

    public static function getExtension()
    {
        $extensionType = [];

       foreach (config('mediaFile.mediaTypeService') as $service)
       {
           $extensionType [] = join(',', $service['extensions']);
       }
        return join(',', $extensionType);
    }
}
