<div class="mb-4">
    <select name="{{ $name }}" class="{{ $class }}" {{ $attributes }}>
        {{ $slot }}
    </select>
    <x-validation-error field="{{ $name }}"/>
</div>
