<?php


namespace Mdh\User\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Mdh\Course\Models\Course;
use Mdh\Course\Rules\ValidTeacher;
use Mdh\User\Models\User;


class UpdateUserPhotoRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }


    public function rules()
    {
        return [
            'userPhoto' => ['required','mimes:jpg,png,jpeg'],
        ];
    }
}
