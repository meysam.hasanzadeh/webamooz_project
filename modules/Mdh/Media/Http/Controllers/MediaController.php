<?php


namespace Mdh\Media\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mdh\Media\Models\Media;
use Mdh\Media\Services\MediaFileService;


class MediaController extends Controller
{
    public function download(Media $media, Request $request)
    {
        if (!$request->hasValidSignature())
        {
            abort(401);
        }

        return MediaFileService::stream($media);
    }
}
