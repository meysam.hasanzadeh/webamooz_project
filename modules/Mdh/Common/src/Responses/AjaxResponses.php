<?php


namespace Mdh\Common\Responses;


use Illuminate\Http\Response;

class AjaxResponses
{
    public static function success($body = "عملیات موفق بود")
    {
        return response()->json(["message"=>$body], Response::HTTP_OK);
    }

    public static function FailedResponse()
    {
        return response()->json(["message"=> "عملیات ناموفق بود"], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
