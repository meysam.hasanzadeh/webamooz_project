<?php


namespace Mdh\Media\Services;


use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Mdh\Media\Contracts\FileServiceContract;
use Mdh\Media\Models\Media;

class ImageFileService extends DefaultFileService implements FileServiceContract
{
    protected static $sizes = ['300','600'];

    public static function upload(UploadedFile $file, string $fileName, string $dir): array
    {
        Storage::putFileAs($dir, $file,$fileName.'.'.$file->getClientOriginalExtension());
        $path = $dir.'\\'.$fileName.'.'.$file->getClientOriginalExtension();
        return self::resize(Storage::path($path), $dir, $fileName, $file->getClientOriginalExtension());
    }


    private static function resize($img, $dir, $fileName, $extension)
    {
        $img = Image::make($img);
        $imgs['original'] = $fileName.'.'.$extension;

        foreach (self::$sizes as $size) {
            $imgs[$size] = $fileName.'_'.$size.'.'.$extension;
            $img->resize($size,null, function ($aspect){
                $aspect->aspectRatio();
            })->save(Storage::path($dir).$fileName.'_'.$size.'.'.$extension);
        }
        return $imgs;
    }

    public static function thumb(Media $media)
    {
        return '/storage/'.$media->files[300];
    }

    static function getFileName()
    {
        return (static::$media->is_private ? 'private/' : 'public/').static::$media->files['original'];
    }
}
