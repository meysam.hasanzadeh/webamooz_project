<?php

namespace Mdh\User\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Mdh\Course\Models\Course;
use Mdh\Course\Models\Lesson;
use Mdh\Course\Models\Season;
use Mdh\Media\Models\Media;
use Mdh\Payment\Models\Payment;
use Mdh\RolePermissions\Models\Role;
use Mdh\User\Notifications\ResetPasswordRequestNotification;
use Mdh\User\Notifications\VerifyMailNotification;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, HasRoles;

    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';
    const STATUS_BAN = 'ban';

    public static array $statuses = [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_BAN];

    public static array $defaultUsers = [
        ['name' => 'admin', 'email' => 'admin@yahoo.com', 'password' => 'demo', 'role' => Role::ROLE_SUPER_ADMIN]
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'mobile'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyMailNotification());
    }

    public function sendPasswordVerifyCodeNotification(){
        $this->notify(new ResetPasswordRequestNotification());
    }

    public function image()
    {
        return $this->belongsTo(Media::class, 'image_id');
    }

    public function courses()
    {
        return $this->hasMany(Course::class, 'teacher_id');
    }

    public function purchases()
    {
        return $this->belongsToMany(Payment::class, 'course_user', 'user_id', 'course_id');
    }

    public function payments()
    {
        return $this->hasMany(Payment::class,'buyer_id');
    }

    public function seasons()
    {
        return $this->hasMany(Season::class);
    }

    public function lessons()
    {
        return $this->hasMany(Lesson::class);
    }

    public function studentsCount($tutor)
    {
        return DB::table('courses')->where('teacher_id',$this->id)
                ->join('course_user','courses.id',"=","course_user.course_id")
                ->count();

        // my solution
//        $count_student = 0;
//        foreach ($tutor->courses as $course)
//        {
//            $count_student += count($course->students);
//        }
//
//        return $count_student;
    }
}
