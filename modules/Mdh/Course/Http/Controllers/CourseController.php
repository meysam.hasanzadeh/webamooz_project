<?php


namespace Mdh\Course\Http\Controllers;


use App\Http\Controllers\Controller;
use Mdh\Category\Repositories\CategoryRepo;
use Mdh\Common\Responses\AjaxResponses;
use Mdh\Course\Http\Requests\CourseRequest;
use Mdh\Course\Models\Course;
use Mdh\Course\Repositories\CourseRepo;
use Mdh\Course\Repositories\LessonRepo;
use Mdh\Media\Services\MediaFileService;
use Mdh\Payment\Gateways\Gateway;
use Mdh\Payment\Services\PaymentService;
use Mdh\RolePermissions\Models\Permission;
use Mdh\User\Repositories\UserRepo;



class CourseController extends Controller
{

    /**
     * @var CourseRepo
     */
    private CourseRepo $course_repo;

    public function __construct(CourseRepo $course_repo)
    {
        $this->course_repo = $course_repo;
    }

    public function index()
    {
        $this->authorize('index', Course::class);
        if (auth()->user()->hasAnyPermission([Permission::PERMISSION_MANAGE_COURSES, Permission::PERMISSION_SUPER_ADMIN]))
        {
            $courses = $this->course_repo->paginate();
        }else {
            $courses = $this->course_repo->findByUserId(auth()->user()->id);
        }
        return view('Courses::index', compact('courses'));
    }

    public function create(UserRepo $userRepo, CategoryRepo $categoryRepo)
    {
        $this->authorize('create', Course::class);

        $teachers = $userRepo->getTeachers();
        $categories = $categoryRepo->all();

        return view('Courses::create',compact('teachers','categories'));
    }

    public function store(CourseRequest $request)
    {

        $request->request->add(['banner_id'=> MediaFileService::publicUpload($request->file('image'))->id]);
        $this->course_repo->store($request);
        return redirect(route('courses.index'));
    }

    public function edit($id, UserRepo $userRepo, CategoryRepo $categoryRepo)
    {
        $course = $this->course_repo->findById($id);
        $teachers = $userRepo->getTeachers();
        $categories = $categoryRepo->all();

        return view('Courses::edit', compact('course','teachers','categories'));
    }

    public function details($id, CourseRepo $course_repo, LessonRepo $lessonRepo)
    {
        $course = $course_repo->findById($id);
        $lessons = $lessonRepo->getLessonCourse($course->id);

        $this->authorize('details', $course);

        return view('Courses::details', compact('course', 'lessons'));
    }

    public function downloadLinks($courseId)
    {
        $course = $this->course_repo->findById($courseId);
        $this->authorize('download',$course);

        return implode("<br>",$course->downloadLinks());
    }

    public function update($id, CourseRequest $request)
    {
        $course = $this->course_repo->findById($id);
        $this->authorize('edit', $course);

        if($request->hasFile('image'))
        {
            $request->request->add(['banner_id'=> MediaFileService::publicUpload($request->file('image'))->id]);

            if (isset($course->banner))
            {
                $course->banner->delete();;
            }

        }else {
            $request->request->add(['banner_id'=> $course->banner_id]);
        }
        $this->course_repo->update($id, $request);

        return redirect(route('courses.index'));
    }

    public function destroy($id, CourseRepo $courseRepo)
    {
        $this->authorize('delete');
        $course = $courseRepo->findById($id);

        if (isset($course->banner))
        {
            $course->banner->delete();
        }

        $courseRepo->delete($id);

        return AjaxResponses::success();
    }

    public function accept($id)
    {
        $this->authorize('change_confirmation_status', Course::class);
        if($this->course_repo->updateConfirmationStatus($id, Course::CONFIRMATION_STATUS_ACCEPTED))
        {
            return AjaxResponses::success();
        }

        return AjaxResponses::FailedResponse();
    }

    public function reject($id)
    {
        $this->authorize('change_confirmation_status', Course::class);
        if($this->course_repo->updateConfirmationStatus($id, Course::CONFIRMATION_STATUS_REJECTED))
        {
            return AjaxResponses::success();
        }

        return AjaxResponses::FailedResponse();
    }

    public function lock($id)
    {
        $this->authorize('change_confirmation_status', Course::class);
        if($this->course_repo->updateStatus($id, Course::STATUS_LOCKED))
        {
            return AjaxResponses::success();
        }

        return AjaxResponses::FailedResponse();
    }

    public function buy($courseId)
    {
        $course = $this->course_repo->findById($courseId);

        if (!$this->courseCanPurchased($course))
        {
            return back();
        }

        if (!$this->authUserCanPurchaseCourse($course))
        {
            return back();
        }

        $amount = $course->getFinalPrice();
        if ($amount <= 0)
        {
            $this->course_repo->addStudentToCourse($course, auth()->id());
            newFeedback('عملیات موفق','شما در دوره ثبت نام شدید','success');
            return redirect($course->path());
        }
        $payment = PaymentService::generate($amount,$course,auth()->user());

        resolve(Gateway::class)->redirect($payment->invoice_id);

    }

    private function courseCanPurchased(Course $course)
    {
        if ($course->type == Course::TYPE_FREEE){
            newFeedback('عملیات ناموفق', 'دوره های رایگان قابل خریداری نیستند !','error');
            return false;
        }
        if ($course->confirmation_status != Course::CONFIRMATION_STATUS_ACCEPTED){
            newFeedback('عملیات ناموفق', 'دوره انتخابی شما هنوز تایید نشده است!','error');
            return false;
        }
        if ($course->status == Course::STATUS_LOCKED){
            newFeedback('عملیات ناموفق', 'این دوره قفل شده است و فعلا قابل خریداری نیست!','error');
            return false;
        }

        return true;
    }

    private function authUserCanPurchaseCourse(Course $course)
    {
        if(auth()->user()->id == $course->teacher_id){
            newFeedback('عملیات ناموفق', 'شما مدرس این دوره هستید!','error');
            return false;
        }

        if (auth()->user()->can('download',$course)){
            newFeedback('عملیات ناموفق', 'شما به این دوره دسترسی دارید!','error');
            return false;
        }

        return true;
    }
}
